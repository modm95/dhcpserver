var indexSectionsWithContent =
{
  0: "abcdefghilmnoprsuvwxy",
  1: "dlnpr",
  2: "cdilmr",
  3: "bcdfghilmprsuw",
  4: "abcdfghilnoprsvxy",
  5: "dlnpr",
  6: "blrs",
  7: "bcdhlms",
  8: "fl"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Modules",
  8: "Pages"
};

