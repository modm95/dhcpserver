var searchData=
[
  ['raw_5fmsg',['raw_msg',['../structraw__msg.html',1,'raw_msg'],['../dhcp__server_8h.html#a647d1415e6c9917fa52df77a5b66dc18',1,'raw_msg():&#160;dhcp_server.h']]],
  ['readme_2emd',['README.md',['../README_8md.html',1,'']]],
  ['rebind',['rebind',['../structnet__temp__config.html#ab437b7d5db273049ef764eb1da37d2d3',1,'net_temp_config::rebind()'],['../structnetwork__config.html#a77eafab17ba245bf99bd39110251ad01',1,'network_config::rebind()']]],
  ['release_5fbind',['release_bind',['../dhcp__server_8c.html#a5b39a8293619c401af7c9d12dafc7d8f',1,'dhcp_server.c']]],
  ['renew',['renew',['../structnet__temp__config.html#a4985f81d168e7df9b494d78b3e403752',1,'net_temp_config::renew()'],['../structnetwork__config.html#a1f5e466ecaa7c9a8dfb874a72a9edb04',1,'network_config::renew()']]],
  ['reply_5fport',['REPLY_PORT',['../dhcp__server_8h.html#a18413f1f12ed4cc370da768cc95ffb5c',1,'dhcp_server.h']]],
  ['request',['request',['../check__pkt_8c.html#a7d0de4f9b5a24f837931450769e6fd31',1,'request():&#160;check_pkt.c'],['../check__pktbuild_8c.html#a7d0de4f9b5a24f837931450769e6fd31',1,'request():&#160;check_pktbuild.c']]],
  ['retrieve_5finfo',['retrieve_info',['../ip__handler_8c.html#a83a12159073ec2c32cf9dadc514e1776',1,'retrieve_info(char *column, char value[16]):&#160;ip_handler.c'],['../ip__handler_8h.html#a83a12159073ec2c32cf9dadc514e1776',1,'retrieve_info(char *column, char value[16]):&#160;ip_handler.c']]],
  ['router',['router',['../structnetwork__config.html#a1f936a3301895f5a637b5d6456ad0090',1,'network_config']]]
];
