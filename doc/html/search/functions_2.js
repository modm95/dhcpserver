var searchData=
[
  ['deserialize',['deserialize',['../dhcp__packet_8c.html#a5289e1ac18077b76ddd065655295b42a',1,'deserialize(char buffer[], int offset, int length):&#160;dhcp_packet.c'],['../dhcp__packet_8h.html#a5289e1ac18077b76ddd065655295b42a',1,'deserialize(char buffer[], int offset, int length):&#160;dhcp_packet.c']]],
  ['deserialize_5fpacket_5ferror',['deserialize_packet_error',['../dhcp__packet_8c.html#a3353483e20035dfbfed41f769b9891f9',1,'dhcp_packet.c']]],
  ['dispatch',['dispatch',['../dhcp__server_8c.html#a6dba84090374b62cef13913a3e604d0b',1,'dispatch(dhcp_packet *request):&#160;dhcp_server.c'],['../dhcp__server_8h.html#affff5a84dba6c9ae5081c82319df6653',1,'dispatch(struct dhcp_packet *request):&#160;dhcp_server.c']]],
  ['dispatch_5ferror',['dispatch_error',['../dhcp__server_8c.html#ae7146342d036479f68fbcea06645b248',1,'dhcp_server.c']]],
  ['do_5facknowledgement',['do_acknowledgement',['../dhcp__server_8c.html#ab6635bb6eb2c1f4827acd8d52f895f7c',1,'do_acknowledgement(dhcp_packet *request):&#160;dhcp_server.c'],['../dhcp__server_8h.html#a3357b19f2a12ec4ba701c8edbcbdc9d2',1,'do_acknowledgement(struct dhcp_packet *request):&#160;dhcp_server.c']]],
  ['do_5fdecline',['do_decline',['../dhcp__server_8c.html#a35ddeccc7a017c206a3731e21012879d',1,'do_decline(dhcp_packet *request):&#160;dhcp_server.c'],['../dhcp__server_8h.html#a9a38130e3cca2c196bf8db52641f18f8',1,'do_decline(struct dhcp_packet *request):&#160;dhcp_server.c']]],
  ['do_5finform',['do_inform',['../dhcp__server_8c.html#a5ee84ad84560509ffc38e020e3cd7ac0',1,'do_inform(dhcp_packet *request):&#160;dhcp_server.c'],['../dhcp__server_8h.html#a17109f18afe2a242b0da3e9acd8ec80f',1,'do_inform(struct dhcp_packet *request):&#160;dhcp_server.c']]],
  ['do_5foffer',['do_offer',['../dhcp__server_8c.html#a334d5142bd80116a1d12ceccc2592e4a',1,'do_offer(dhcp_packet *request):&#160;dhcp_server.c'],['../dhcp__server_8h.html#afe62be8f0872f3bcc7d0b899865afa8c',1,'do_offer(struct dhcp_packet *request):&#160;dhcp_server.c']]],
  ['do_5frelease',['do_release',['../dhcp__server_8c.html#af2569b43db0a06f0b5fe4dc322ecbc2a',1,'do_release(dhcp_packet *request):&#160;dhcp_server.c'],['../dhcp__server_8h.html#a294d4af998b10b42cc4c298a60d9f30b',1,'do_release(struct dhcp_packet *request):&#160;dhcp_server.c']]]
];
