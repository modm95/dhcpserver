var searchData=
[
  ['handle_5fmsg',['handle_msg',['../dhcp__server_8c.html#a74970c42d7db56f5311ffc13f00b7b73',1,'handle_msg(raw_msg *msg):&#160;dhcp_server.c'],['../dhcp__server_8h.html#a74970c42d7db56f5311ffc13f00b7b73',1,'handle_msg(raw_msg *msg):&#160;dhcp_server.c']]],
  ['hardware_5faddress',['hardware_address',['../structnetwork__config.html#a66e37545052bf5b21e1deb99b2928d1c',1,'network_config']]],
  ['hlen',['hlen',['../structdhcp__packet.html#af14a283624a896e62a18b16399051945',1,'dhcp_packet']]],
  ['hops',['hops',['../structdhcp__packet.html#aaa44ff360bdff5302863eb609338a7b2',1,'dhcp_packet']]],
  ['htype',['htype',['../structdhcp__packet.html#a7a2d2b2cd42646d1fe756c8afafc5e28',1,'dhcp_packet::htype()'],['../group__HTYPE.html',1,'(Global Namespace)']]],
  ['htype_5fether',['HTYPE_ETHER',['../group__HTYPE.html#gaa4534fc7c0bf77c249b4d8cb51dfe4ae',1,'dhcp_packet.h']]],
  ['htype_5ffddi',['HTYPE_FDDI',['../group__HTYPE.html#ga5cd1a2fabf03746aa09d92dec6195fbf',1,'dhcp_packet.h']]],
  ['htype_5fieee1394',['HTYPE_IEEE1394',['../group__HTYPE.html#gad99e3d6b40961dc804f56a84858326fa',1,'dhcp_packet.h']]],
  ['htype_5fieee802',['HTYPE_IEEE802',['../group__HTYPE.html#ga37e7fa38d0a696c926e18b9734ed6c1a',1,'dhcp_packet.h']]]
];
