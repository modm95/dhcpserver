/**
* @file check_main.c
* @brief Unit testing main execution
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.26
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include <stdlib.h>
#include <check.h>

extern Suite *build_suite();    //every test set is put in a Suite, and every Suite is run by this main

int main(void){

    int number_failed;
	Suite *s = build_suite();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
	
}
