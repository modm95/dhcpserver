/**
* @file check_pkt.c
* @brief Unit testing for packet manipulation
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.27
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <check.h>

#include "dhcp_server.h"
#include "ip_handler.h"

net_temp_config db_config = {
	
	.server = "192.168.1.1",
	.lease = 0,
	.renew = 0,
	.rebind = 0,
	.gateway = "192.168.1.1",
	.netmask = "255.255.255.0",
	.dns1 = "8.8.8.8",
	.dns2 = "8.8.4.4",
	.pool = "192.168.1.2-192.168.1.3"
	
};

dhcp_packet request = {
	
	.op = 1,
	.hops = 0,
    .xid = {0x59, 0x57, 0xF4, 0xCA},
	.secs = 0,
	.flags = {0x80, 0x00},
	.chaddr = {0x4D, 0x5C, 0x3A, 0x40, 0x32, 0xCC},
	.ciaddr = {0},
	.yiaddr = {0},
	.siaddr = {0},
	.giaddr = {0}
		
};

//test free packet functionality
START_TEST(test_freepkt_create){    
    
	dhcp_packet *pkt = calloc(1, sizeof(*pkt));	
	pkt->options = calloc(1, sizeof(*pkt));
	pkt->options->value = calloc(1, 16);

	ck_assert_int_eq(free_packet(pkt), 0);

}
END_TEST

//test serialization and deserialization
START_TEST(test_serialization_create){     
	
	strncpy(db_name, "table.db", 8);
	network_config config = {0};
	memcpy(config.hardware_address, request.chaddr, 16);
	create_allocation_base(&db_config);
    dhcp_packet *response = do_offer(&request);
	remove("table.db");
	
	char buffer[DHCP_MAX_MTU];
	int length = serialize(response, buffer, DHCP_MAX_MTU);
	
	ck_assert_int_gt(length, 0);
	
	response = deserialize(buffer, 0, length);
	
	ck_assert_int_eq(response->xid[0], (char)0x59);
	ck_assert_int_eq(response->xid[1], (char)0x57);
	ck_assert_int_eq(response->xid[2], (char)0xF4);
	ck_assert_int_eq(response->xid[3], (char)0xCA);
	ck_assert_int_eq(response->flags[0], (char)0x80);
	ck_assert_int_eq(response->flags[1], (char)0x00);
	ck_assert_int_eq(response->chaddr[0], (char)0x4D);
	ck_assert_int_eq(response->chaddr[1], (char)0x5C);
	ck_assert_int_eq(response->chaddr[2], (char)0x3A);
	ck_assert_int_eq(response->chaddr[3], (char)0x40);
	ck_assert_int_eq(response->chaddr[4], (char)0x32);
	ck_assert_int_eq(response->chaddr[5], (char)0xCC);
	
	free_packet(response);
	
}
END_TEST



Suite *build_suite(void){
	
    Suite *s = suite_create("DHCP");
    TCase *tc_pkt = tcase_create("Packet");
	
	tcase_add_test(tc_pkt, test_freepkt_create);
	tcase_add_test(tc_pkt, test_serialization_create);
	
    suite_add_tcase(s, tc_pkt);

    return s;
			
}