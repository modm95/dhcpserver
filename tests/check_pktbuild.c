/**
* @file check_pktbuild.c
* @brief Unit testing for packet generation
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.27
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include <stdio.h>
#include <check.h>

#include "dhcp_server.h"
#include "ip_handler.h"

net_temp_config db_config = {
	
	.server = "192.168.1.1",
	.lease = 1000,
	.renew = 10,
	.rebind = 100,
	.gateway = "192.168.1.1",
	.netmask = "255.255.255.0",
	.dns1 = "8.8.8.8",
	.dns2 = "8.8.4.4",
	.pool = "192.168.1.2-192.168.1.3"
	
};

dhcp_packet request = {
	
    .op = 1,
	.hops = 0,
    .xid = {0x59, 0x57, 0xF4, 0xCA},
	.secs = 0,
	.flags = {0x80, 0x00},
	.chaddr = {0x4D, 0x5C, 0x3A, 0x40, 0x32, 0xCC},
	.ciaddr = {192, 168, 1, 2},
	.yiaddr = {0},
	.siaddr = {0},
	.giaddr = {0}

};

//test offer
START_TEST(test_offer_builder){
    	
	strncpy(db_name, "table.db", 8);
	network_config config = {0};
	memcpy(config.hardware_address, request.chaddr, 16);
	create_allocation_base(&db_config);
    dhcp_packet *response = do_offer(&request);
	
	ck_assert_int_eq(response->op, 2);
	ck_assert_int_eq(response->hops, 1);
	ck_assert_int_eq(response->xid[0], (char)0x59);
	ck_assert_int_eq(response->xid[1], (char)0x57);
	ck_assert_int_eq(response->xid[2], (char)0xF4);
	ck_assert_int_eq(response->xid[3], (char)0xCA);
	ck_assert_int_eq(response->flags[0], (char)0x80);
	ck_assert_int_eq(response->flags[1], (char)0x00);
	ck_assert_int_eq(response->chaddr[0], (char)0x4D);
	ck_assert_int_eq(response->chaddr[1], (char)0x5C);
	ck_assert_int_eq(response->chaddr[2], (char)0x3A);
	ck_assert_int_eq(response->chaddr[3], (char)0x40);
	ck_assert_int_eq(response->chaddr[4], (char)0x32);
	ck_assert_int_eq(response->chaddr[5], (char)0xCC);
	ck_assert_int_eq(response->yiaddr[0], (char)192);
	ck_assert_int_eq(response->yiaddr[1], (char)168);
	ck_assert_int_eq(response->yiaddr[2], (char)1);
	ck_assert_int_eq(response->yiaddr[3], (char)2);
	dhcp_option *packet_type = response->options;
	dhcp_option *server_identifier = packet_type->next;
	dhcp_option *lease_time = server_identifier->next;
	dhcp_option *renew_time = lease_time->next;
	dhcp_option *rebind_time = renew_time->next;
	dhcp_option *router = rebind_time->next;
	dhcp_option *netmask = router->next;
	dhcp_option *dns_server = netmask->next;
	ck_assert_int_eq(*(char *)packet_type->value, DHCP_OFFER);
	ck_assert_int_eq(*(char *)server_identifier->value, (char)192);
	ck_assert_int_eq(*(char *)(server_identifier->value + 1), (char)168);
	ck_assert_int_eq(*(char *)(server_identifier->value + 2), (char)1);
	ck_assert_int_eq(*(char *)(server_identifier->value + 3), (char)1);
    ck_assert_int_eq(ntohl(*(uint32_t *)lease_time->value), 1000);
	ck_assert_int_eq(ntohl(*(uint32_t *)renew_time->value), 10);
	ck_assert_int_eq(ntohl(*(uint32_t *)rebind_time->value), 100);
	ck_assert_int_eq(*(char *)router->value, (char)192);
	ck_assert_int_eq(*(char *)(router->value + 1), (char)168);
	ck_assert_int_eq(*(char *)(router->value + 2), (char)1);
	ck_assert_int_eq(*(char *)(router->value + 3), (char)1);
	ck_assert_int_eq(*(char *)netmask->value, (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 1), (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 2), (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 3), (char)0);
 	ck_assert_int_eq(*(char *)dns_server->value, (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 1), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 2), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 3), (char)8);
    ck_assert_int_eq(*(char *)(dns_server->value + 4), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 5), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 6), (char)4);
	ck_assert_int_eq(*(char *)(dns_server->value + 7), (char)4);
	
	free_packet(response);
    remove("table.db");
			
	
}
END_TEST

//test ack
START_TEST(test_acknowledgement_builder){
	
	strncpy(db_name, "table.db", 8);
	network_config config = {0};
	memcpy(config.hardware_address, request.chaddr, 16);	
	create_allocation_base(&db_config);
	do_offer(&request);
	dhcp_packet *response = do_acknowledgement(&request);
	
	ck_assert_int_eq(response->op, 2);
	ck_assert_int_eq(response->hops, 1);
	ck_assert_int_eq(response->xid[0], (char)0x59);
	ck_assert_int_eq(response->xid[1], (char)0x57);
	ck_assert_int_eq(response->xid[2], (char)0xF4);
	ck_assert_int_eq(response->xid[3], (char)0xCA);
	ck_assert_int_eq(response->flags[0], (char)0x80);
	ck_assert_int_eq(response->flags[1], (char)0x00);
	ck_assert_int_eq(response->chaddr[0], (char)0x4D);
	ck_assert_int_eq(response->chaddr[1], (char)0x5C);
	ck_assert_int_eq(response->chaddr[2], (char)0x3A);
	ck_assert_int_eq(response->chaddr[3], (char)0x40);
	ck_assert_int_eq(response->chaddr[4], (char)0x32);
	ck_assert_int_eq(response->chaddr[5], (char)0xCC);
	dhcp_option *packet_type = response->options;
	dhcp_option *server_identifier = packet_type->next;
	dhcp_option *lease_time = server_identifier->next;
	dhcp_option *renew_time = lease_time->next;
	dhcp_option *rebind_time = renew_time->next;
	dhcp_option *router = rebind_time->next;
	dhcp_option *netmask = router->next;
	dhcp_option *dns_server = netmask->next;	
	ck_assert_int_eq(*(char *)packet_type->value, DHCP_ACK);
	ck_assert_int_eq(*(char *)server_identifier->value, (char)192);
	ck_assert_int_eq(*(char *)(server_identifier->value + 1), (char)168);
	ck_assert_int_eq(*(char *)(server_identifier->value + 2), (char)1);
	ck_assert_int_eq(*(char *)(server_identifier->value + 3), (char)1);
    ck_assert_int_eq(ntohl(*(uint32_t *)lease_time->value), 1000);
	ck_assert_int_eq(ntohl(*(uint32_t *)renew_time->value), 10);
	ck_assert_int_eq(ntohl(*(uint32_t *)rebind_time->value), 100);
	ck_assert_int_eq(*(char *)router->value, (char)192);
	ck_assert_int_eq(*(char *)(router->value + 1), (char)168);
	ck_assert_int_eq(*(char *)(router->value + 2), (char)1);
	ck_assert_int_eq(*(char *)(router->value + 3), (char)1);
	ck_assert_int_eq(*(char *)netmask->value, (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 1), (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 2), (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 3), (char)0);
 	ck_assert_int_eq(*(char *)dns_server->value, (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 1), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 2), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 3), (char)8);
    ck_assert_int_eq(*(char *)(dns_server->value + 4), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 5), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 6), (char)4);
	ck_assert_int_eq(*(char *)(dns_server->value + 7), (char)4);
	
	free_packet(response);
	remove("table.db");	
	
}
END_TEST

//test inform ack
START_TEST(test_inform_builder){

    strncpy(db_name, "table.db", 8);
	network_config config = {0};
	memcpy(config.hardware_address, request.chaddr, 16);	
	create_allocation_base(&db_config);
	do_offer(&request);
	do_acknowledgement(&request);
    dhcp_packet *response = do_inform(&request);

    ck_assert_int_eq(response->op, 2);
    ck_assert_int_eq(response->hops, 1);
    ck_assert_int_eq(response->xid[0], (char)0x59);
    ck_assert_int_eq(response->xid[1], (char)0x57);
    ck_assert_int_eq(response->xid[2], (char)0xF4);
    ck_assert_int_eq(response->xid[3], (char)0xCA);
    ck_assert_int_eq(response->flags[0], (char)0x80);
    ck_assert_int_eq(response->flags[1], (char)0x00);
    ck_assert_int_eq(response->chaddr[0], (char)0x4D);
    ck_assert_int_eq(response->chaddr[1], (char)0x5C);
    ck_assert_int_eq(response->chaddr[2], (char)0x3A);
    ck_assert_int_eq(response->chaddr[3], (char)0x40);
    ck_assert_int_eq(response->chaddr[4], (char)0x32);
    ck_assert_int_eq(response->chaddr[5], (char)0xCC);
	ck_assert_int_eq(response->yiaddr[0], (char)0);
	ck_assert_int_eq(response->yiaddr[1], (char)0);
	ck_assert_int_eq(response->yiaddr[2], (char)0);
	ck_assert_int_eq(response->yiaddr[3], (char)0);
	dhcp_option *packet_type = response->options;
	dhcp_option *server_identifier = packet_type->next;
	dhcp_option *renew_time = server_identifier->next;
	dhcp_option *rebind_time = renew_time->next;
	dhcp_option *router = rebind_time->next;
	dhcp_option *netmask = router->next;
	dhcp_option *dns_server = netmask->next;	
	ck_assert_int_eq(*(char *)packet_type->value, DHCP_ACK);
	ck_assert_int_eq(*(char *)server_identifier->value, (char)192);
	ck_assert_int_eq(*(char *)(server_identifier->value + 1), (char)168);
	ck_assert_int_eq(*(char *)(server_identifier->value + 2), (char)1);
	ck_assert_int_eq(*(char *)(server_identifier->value + 3), (char)1);
	ck_assert_int_eq(ntohl(*(uint32_t *)renew_time->value), 10);
	ck_assert_int_eq(ntohl(*(uint32_t *)rebind_time->value), 100);
	ck_assert_int_eq(*(char *)router->value, (char)192);
	ck_assert_int_eq(*(char *)(router->value + 1), (char)168);
	ck_assert_int_eq(*(char *)(router->value + 2), (char)1);
	ck_assert_int_eq(*(char *)(router->value + 3), (char)1);
	ck_assert_int_eq(*(char *)netmask->value, (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 1), (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 2), (char)255);
	ck_assert_int_eq(*(char *)(netmask->value + 3), (char)0);
 	ck_assert_int_eq(*(char *)dns_server->value, (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 1), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 2), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 3), (char)8);
    ck_assert_int_eq(*(char *)(dns_server->value + 4), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 5), (char)8);
	ck_assert_int_eq(*(char *)(dns_server->value + 6), (char)4);
	ck_assert_int_eq(*(char *)(dns_server->value + 7), (char)4);
	
	free_packet(response);
	remove("table.db");
				
}
END_TEST

Suite *build_suite(void){
	
    Suite *s = suite_create("DHCP");
    TCase *tc_bpkt = tcase_create("PacketBuild");
	
	tcase_add_test(tc_bpkt, test_offer_builder);
	tcase_add_test(tc_bpkt, test_acknowledgement_builder);
	tcase_add_test(tc_bpkt, test_inform_builder);
	
    suite_add_tcase(s, tc_bpkt);

    return s;
			
}