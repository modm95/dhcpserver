/**
* @file check_db.c
* @brief Unit testing for database functions
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.26
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include <stdio.h>
#include <check.h>

#include "dhcp_server.h"
#include "ip_handler.h"

net_temp_config db_config = {
	
	.server = "192.168.1.1",
	.lease = 0,
	.renew = 0,
	.rebind = 0,
	.gateway = "192.168.1.1",
	.netmask = "255.255.255.0",
	.dns1 = "8.8.8.8",
	.dns2 = "8.8.4.4",
	.pool = "192.168.1.2-192.168.1.3"
	
};

net_temp_config db_config_wrong = {
	
	.server = "192.168.1.1",
	.lease = 0,
	.renew = 0,
	.rebind = 0,
	.gateway = "192.168.1.1",
	.netmask = "255.255.255.0",
	.dns1 = "8.8.8.8",
	.dns2 = "8.8.4.4",
	.pool = "193.168.1.2-192.168.1.3"
	
};

//test correct conversion of mac to integer
START_TEST(test_get_intmac){

    network_config config = {0};
    char chaddr[16] = {0xF3, 0x0F, 0xEF, 0xC5, 0xAE, 0x22};
    memcpy(config.hardware_address, chaddr, 16);
	
    ck_assert_int_eq(get_uint_mac(&config), 267249772768802);

}
END_TEST

//test correct creation of database with correct configuration
START_TEST(test_database_creator){
	
	strncpy(db_name, "table.db", 8);
	
	ck_assert_int_eq(create_allocation_base(&db_config),0);
	remove("table.db");

}
END_TEST

//if configuration is incorrect, database creation has to fail
START_TEST(test_database_creator_fail){
	
	strncpy(db_name, "table.db", 8);
	
	ck_assert_int_eq(create_allocation_base(&db_config_wrong), -1);
	remove("table.db");

}
END_TEST

//test allocation
START_TEST(test_database_allocator){
	
    strncpy(db_name, "table.db", 8);
    network_config config = {0};
    char chaddr[16] = {0xF3, 0x0F, 0xEF, 0xC5, 0xAE, 0x22};
    memcpy(config.hardware_address, chaddr, 16);	
	create_allocation_base(&db_config);
	
	ck_assert_int_eq(ip_allocator(&config), 0);
	remove("table.db");

}
END_TEST

//test confirmation
START_TEST(test_database_confirmer){
    
	strncpy(db_name, "table.db", 8);
    network_config config = {0};
    char chaddr[16] = {0xF3, 0x0F, 0xEF, 0xC5, 0xAE, 0x22};
    memcpy(config.hardware_address, chaddr, 16);	
	create_allocation_base(&db_config);
	ip_allocator(&config);
	
	ck_assert_int_eq(ip_confirmer(&config), 0);
	remove("table.db");

}
END_TEST

//test releasing
START_TEST(test_database_releaser){
	
    strncpy(db_name, "table.db", 8);
    network_config config = {0};
    char chaddr[16] = {0xF3, 0x0F, 0xEF, 0xC5, 0xAE, 0x22};
    memcpy(config.hardware_address, chaddr, 16);	
	create_allocation_base(&db_config);
	ip_allocator(&config);
	
	ck_assert_int_eq(ip_releaser(&config), 0);
	remove("table.db");

}
END_TEST

//test blocking
START_TEST(test_database_blocker){

    strncpy(db_name, "table.db", 8);
    network_config config = {0};
    char chaddr[16] = {0xF3, 0x0F, 0xEF, 0xC5, 0xAE, 0x22};
    memcpy(config.hardware_address, chaddr, 16);	
	create_allocation_base(&db_config);
	ip_allocator(&config);
	
	ck_assert_int_eq(ip_blocker(&config), 0);
	remove("table.db");

}
END_TEST

Suite *build_suite(void){
	
    Suite *s = suite_create("DHCP");
    TCase *tc_db = tcase_create("Database");
	
	tcase_add_test(tc_db, test_get_intmac);
	tcase_add_test(tc_db, test_database_creator);
	tcase_add_test(tc_db, test_database_creator_fail);
	tcase_add_test(tc_db, test_database_allocator);
	tcase_add_test(tc_db, test_database_confirmer);
	tcase_add_test(tc_db, test_database_releaser);
	tcase_add_test(tc_db, test_database_blocker);	
	
    suite_add_tcase(s, tc_db);

    return s;
			
}
