/**
* @file ip_handler.h
* @brief Header for definition of database management functions
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.13
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#ifndef _IP_HANDLE_H_
#define _IP_HANDLE_H_

/* Needed for temporarily storing pool information */
typedef struct pool_info{
	
	char first[16];
	char last[16];
	
}pool_info;

/**
*
* @brief Gets mac from client in integer format
*
* @param config A pointer to a network_config struct that contains the mac in char array form
*
* @returns The mac in uint64_t format
*
*/
uint64_t get_uint_mac(network_config *config);

/**
* 
* @brief Full name : IP ASCII to Bytes
* @brief Stores the integer parts of an IP address into a string
*
* @param bytes A char array, the destination
* @param ip_address The source string
* 
*/
void ip_atob(char bytes[5], const char *ip_address);

/**
* 
* @brief Parses and checks a pool of IP addresses
*
* @param pool An array that contains the server pool in the format first_ip-last_ip
*
* @returns A pointer to a struct that contanis the first_ip and the last_ip separated
* 
*/
pool_info *parse_pool(char pool[32]);

/**
* 
* @brief Creates a database from network parameters, filling it with ips
*
* @param db_config A pointer to a struct containing different network parameters
*
* @returns An integer, 0 on success, 1 otherwise
* 
*/
int create_allocation_base(net_temp_config *db_config);

/**
* 
* @brief Gets a single field from the network_config table in the database 
*
* @param column A string that is the column where to take the info
* @param value A string where to store the taken info
*
* @returns An integer, 0 on success, 1 otherwise
* 
*/
int retrieve_info(char *column, char value[16]);

/**
* 
* @brief Adds a mac-ip association in the database and takes the network parameters stored in the database
*
* @param config A pointer to a struct containing different network parameters
*
* @returns An integer, 0 on success, 1 otherwise
* 
*/
int ip_allocator(network_config *config);

/**
* 
* @brief Gets a mac-ip association in the database and takes the network parameters stored in the database
*
* @param config A pointer to a struct containing different network parameters
*
* @returns An integer, 0 on success, 1 otherwise
* 
*/
int ip_confirmer(network_config *config);

/**
* 
* @brief Deletes a mac-ip association in the database
*
* @param config A pointer to a struct containing different network parameters
*
* @returns An integer, 0 on success, 1 otherwise
* 
*/
int ip_releaser(network_config *config);

/**
* 
* @brief Prevents the use of a certain ip in the database
*
* @param config A pointer to a struct containing different network parameters
*
* @returns An integer, 0 on success, 1 otherwise
* 
*/
int ip_blocker(network_config *config);

/**
* 
* @brief Allows the reuse of the ip addresses affected by ip_blocker or DHCPRELEASE packets not arrived
*
* @param diff A double that has the meaning of a time difference in seconds
*
* @returns An integer, 0 on success, 1 otherwise
* 
*/
int update_bindings(double diff);

#endif //_IP_HANDLE_H_
