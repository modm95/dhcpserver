/**
* @file dhcp_server.h
* @brief Header for defining characteristics of the DHCP server main loop and functions.
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.13
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#ifndef _DHCP_SERVER_H_
#define _DHCP_SERVER_H_

#include<stdint.h>
#include<netinet/in.h>

#include "dhcp_packet.h"

/* Defines the listen port*/
#define LISTEN_PORT  67

/* Defines the replay port*/
#define REPLY_PORT  68

/* Defines the broadcast address */
#define BROADCAST_ADDRESS  "255.255.255.255"

/** 
*
* @defgroup CONFIG CONFIG strings
* @{
*
*/
#define	CONFIG_SERVER_ADDRESS  "server"
#define CONFIG_LEASE_TIME  "lease_time"
#define	CONFIG_RENEW_TIME  "renew_time"
#define CONFIG_REBIND_TIME  "rebind_time"
#define CONFIG_IP_ALLOCATOR_FILE  "ip_allocator_file"
#define CONFIG_GATEWAY  "gateway"
#define CONFIG_NETMASK  "netmask"
#define CONFIG_DNS1  "dns1"
#define CONFIG_DNS2  "dns2"
#define CONFIG_POOL  "pool"
/**
*
* @}
*
*/

/*Database name container*/
char db_name[256];

/* Struct for the configuration to be put in the database */
typedef struct net_temp_config{
	
	char server[16];
	uint32_t lease;
	uint32_t renew;
	uint32_t rebind;	
	char gateway[16];
	char netmask[16];
	char dns1[16];
	char dns2[16];
	char pool[32];
	
}net_temp_config;

/* Struct for the network configuration elements used for sending a DHCP server message */
typedef struct network_config{
	
	char hardware_address[16];
	char server[5];
	uint32_t lease;
	uint32_t renew;
	uint32_t rebind;
	char ip_address[5];
	char router[5];
	char netmask[5];
	char dns1[5];
	char dns2[5];
	
}network_config;

/* Struct that defines an incoming message */
typedef struct raw_msg{
	
	char buff[DHCP_MAX_MTU];
	uint32_t length;
	struct sockaddr_in address;
	int socket_fd;
	
}raw_msg;

/**
* 
* @brief Prepares the reply to the DHCPDISCOVER packet
*
* @param request A received and deserialized packet
*
* @returns A pointer to a dhcp_packet struct, ready to be serialized and sent
* 
*/
dhcp_packet *do_offer(struct dhcp_packet *request);

/**
* 
* @brief Prepares the reply to the DHCPREQUEST packet
*
* @param request A received and deserialized packet
*
* @returns A pointer to a dhcp_packet struct, ready to be serialized and sent
* 
*/
dhcp_packet *do_acknowledgement(struct dhcp_packet *request);

/**
* 
* @brief Performes operations in order to satisfy DHCPRELEASE request
*
* @param request A received and deserialized packet
*
* @returns A NULL pointer (DHCPRELEASE does not need an answer from server)
* 
*/
dhcp_packet *do_release(struct dhcp_packet *request);

/**
* 
* @brief Prepares the reply to the DHCPINFORM packet
*
* @param request A received and deserialized packet
*
* @returns A pointer to a dhcp_packet struct, ready to be serialized and sent
* 
*/
dhcp_packet *do_inform(struct dhcp_packet *request);

/**
* 
* @brief Performes operations in order to satisfy DHCPDECLINE request
*
* @param request A received and deserialized packet
*
* @returns A NULL pointer (DHCPDECLINE does not need an answer from server)
* 
*/
dhcp_packet *do_decline(struct dhcp_packet *request);

/**
* 
* @brief Starts the server, using parameters from configuration file
*
* @param config_file The name of the configuration file
*
* @returns An integer (shouldn't return anything, the server works in an infinite loop)
* 
*/
int start_server(char *config_file);

/**
* 
* @brief Analyzes the incoming packets and sends back the appropriate reply
*
* @param msg A pointer to the incoming message
* 
*/
void handle_msg(raw_msg *msg);

/**
* 
* @brief Decides which is the the appropriate reply to an incoming message and prepares it
*
* @param request A received and deserialized packet
*
* @returns A pointer to a dhcp_packet struct, ready to be serialized and sent
* 
*/
dhcp_packet *dispatch(struct dhcp_packet *request);

#endif //_DHCP_SERVER_H_