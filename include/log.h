/**
* @file log.h
* @brief Header for defining logging utilities
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.15
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#ifndef _LOG_H_
#define _LOG_H_

/** 
*
* @defgroup LOG Log-integer associations
* @{
*
*/	
#define LOG_INFO  0
#define LOG_WARN  1
#define LOG_ERROR  2
#define LOG_FATAL  3
#define LOG_DEBUG  4
/**
*
* @}
*
*/

/** 
*
* @defgroup STRINGS Strings for log message types
* @{
*
*/	
#define LOG_INFO_STRING	 "INFO"
#define LOG_WARN_STRING	 "WARN"
#define LOG_ERROR_STRING  "ERROR"
#define LOG_FATAL_STRING  "FATAL"
#define LOG_DEBUG_STRING  "DEBUG"
/**
*
* @}
*
*/

/** 
*
* @defgroup LOGCONFIG Strings for log configuration
* @{
*
*/	
#define CONFIG_LOG_ENABLED  "log_enabled"
#define	CONFIG_LOG_LEVEL  "log_level"
#define CONFIG_LOG_FILE_DIR  "log_file"
/**
*
* @}
*
*/

/** 
*
* @defgroup SIZES Max sizes
* @{
*
*/	
#define CONFIG_BUFFER_SIZE  1024
#define LOG_BUFFER_SIZE	 4096	
#define MAX_FILE_PATH  256
/**
*
* @}
*
*/

/* Mnemonic association of log files prefix */
#define LOG_FILE_NAME_PREFIX  "dhcp_log"

/** 
*
* @defgroup LOGFUNCS Log macros functions
* @{
*
*/	
#define INFO(message, ...)  write_log(LOG_INFO, message, ##__VA_ARGS__)
#define WARN(message, ...)  write_log(LOG_WARN, message, ##__VA_ARGS__)
#define ERROR(message, ...)  write_log(LOG_ERROR, message, ##__VA_ARGS__)
#define FATAL(message, ...)  write_log(LOG_FATAL, message, ##__VA_ARGS__)
#define DEBUG(message, ...)  write_log(LOG_DEBUG, message, ##__VA_ARGS__)
/**
*
* @}
*
*/

/* Struct for storing log configuration */
typedef struct log_config{
	
	char log_enabled;
	char log_level;
	char log_file_dir[MAX_FILE_PATH];
	
}log_config;

/**
* 
* @brief Parses the log configuration file and stores it in a log_config struct
*
* @param config_file The name of the configuration file
*
* @returns An integer, 0 on success, -1 on unsuccess
* 
*/
int log_init(char *config_file);

/**
* 
* @brief Writes output in log file and on screen
*
* @param level A char from LOG group (LOG_INFO, LOG_DEBUG, ...)
* @param message The message to be printed
* 
*/
void write_log(char level, char *message, ...);

/**
* 
* @brief Decides the appropriate string associated to the char log_level (ex. LOG_INFO -> LOG_INFO_STRING)
*
* @param log_level A char from LOG group (LOG_INFO, LOG_DEBUG, ...)
*
* @returns The string associated to log_level
* 
*/
char *log_level_string(char log_level);

#endif //_LOG_H_
