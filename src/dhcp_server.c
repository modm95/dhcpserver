/**
* @file dhcp_server.c
* @brief Main server functions and loop.
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.20
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include<errno.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>

#include "dhcp_server.h"
#include "log.h"
#include "ip_handler.h"

/* Database file name */
char db_name[256] = {0};

/**
* 
* @brief Used for error report if server start or message memory allocation failed
*
* @param dhcp_socket A socket
*
* @returns The integer -1
* 
* @details If the socket has been initialized, it's closed, then warns the user
*
*/
int main_error(int dhcp_socket){
	
	if(dhcp_socket != 0)		
	   close(dhcp_socket);
	
	WARN("***error!***==>");
	return -1;
	
}


/**
* 
* @brief Used for error report if dispatch failed for some reason
*
* @returns NULL
*
*/
void* dispatch_error(){
	
	INFO("***Error***, dispatch==>");
	return NULL;
	
}


/**
* 
* @brief Used for releasing binding with sockets and freeing every struct needed for packet processing
*
* @param broadcast_socket A socket
* @param response A dhcp_packet, answer to response
* @param request A request dhcp_packet from client
* @param msg A message
*
* @returns NULL
* 
* @details Closes broadcast socket if opened and frees response, request and msg. It can be used either in case of error or not
*
*/
void release_bind(int broadcast_socket, dhcp_packet *response, dhcp_packet *request, raw_msg *msg){
	
	if(broadcast_socket != 0)				
		close(broadcast_socket);
			
	free_packet(response);
	free_packet(request);
	free(msg);	
	INFO("handle_msg==>");
	
}

/**
* 
* @details Parses the server configuration file, checks if everything is ok and creates the initial database file.
* @details Then performs socket binding to intercept DHCP messages.
* @details After this it enters in an infinite loop where, using the socket, listens for DHCP messages, and when it receives one replies to it.
*
*/
int start_server(char *config_file){
	
	printf("\n");
	INFO("==>start_server, config_file=%s", config_file);
	
	FILE *file = fopen(config_file, "r");
	
	if(file == NULL){
				
		FATAL("***Cannot open config_file!***, start_server==>");
		return -1;
		
	}		
	
	net_temp_config *db_config = calloc(1, sizeof(*db_config));
	
	if(db_config == NULL){
		
		FATAL("***Allocate memory failed! %s(%d)***", strerror(errno), errno);
		return -1;
		
	}
	
	char buffer[CONFIG_BUFFER_SIZE];
    
	//parses conf file line by line and copies info in db_config
	while(!feof(file)){
		
		if(fgets(buffer, CONFIG_BUFFER_SIZE, file) != NULL){	
		    
			buffer[strlen(buffer)-1] = '\0';
			DEBUG("read line from config file: %s", buffer);
					
			int index = 0;
			
			for(; buffer[index] != '='; index++);
										
			buffer[index] = '\0';
			char *value_begin = buffer + index + 1;
			int value_length = (int)strlen(value_begin);
			
			if(value_begin[value_length - 1] == '\n')
			   value_begin[value_length - 1] = '\0';							
			
			if(strcmp(buffer, CONFIG_SERVER_ADDRESS) == 0)
			   strncpy(db_config->server, value_begin, 16);
			
			else if(strcmp(buffer, CONFIG_LEASE_TIME) == 0){
				
				char value[12] = {0};
				strncpy(value, value_begin , 12);
				db_config->lease = (uint32_t)strtol(value, NULL, 0);
				
			}
			
			else if(strcmp(buffer, CONFIG_RENEW_TIME) == 0){
				
                char value[12] = {0} ;
                strncpy(value, value_begin , 12);
                db_config->renew = (uint32_t)strtol(value, NULL, 0);
				
            }
			
			else if(strcmp(buffer, CONFIG_REBIND_TIME) == 0){
				
                char value[12] = {0} ;
                strncpy(value, value_begin , 12);
                db_config->rebind = (uint32_t)strtol(value, NULL, 0);
				
            }
			
            else if(strcmp(buffer, CONFIG_IP_ALLOCATOR_FILE) == 0)
			   strncpy(db_name, value_begin, 256);
		   
            else if(strcmp(buffer, CONFIG_GATEWAY) == 0)
			   strncpy(db_config->gateway, value_begin, 16);
		   
            else if(strcmp(buffer, CONFIG_NETMASK) == 0)
			   strncpy(db_config->netmask, value_begin, 16);
		    
			else if(strcmp(buffer, CONFIG_DNS1) == 0)
			   strncpy(db_config->dns1, value_begin, 16);
		   
		    else if(strcmp(buffer, CONFIG_DNS2) == 0)
			   strncpy(db_config->dns2, value_begin, 16);
		   
		    else if(strcmp(buffer, CONFIG_POOL) == 0)
			   strncpy(db_config->pool, value_begin, 32);
			
		}
		
	}
	
	fclose(file);
				
	int dhcp_socket = 0;

	//checks if all fields are ok
	if(db_config->server == NULL || db_config->lease == 0 || db_config->renew == 0 || db_config->rebind == 0 || db_name == NULL || db_config->gateway == NULL
	   || db_config->gateway == NULL ||  db_config->netmask == NULL || db_config->dns1 == NULL || db_config->dns2 == NULL || db_config->pool == NULL)
	   return main_error(dhcp_socket);
     
	DEBUG("-------DUMP CONFIGURATION----------");
	DEBUG("db_name=%s", db_name);
	DEBUG("db_config->server=%s", db_config->server);
	DEBUG("db_config->lease=%d", db_config->lease);
	DEBUG("db_config->renew=%d", db_config->renew);
	DEBUG("db_config->rebind=%d", db_config->rebind);
	DEBUG("db_config->gateway=%s", db_config->gateway);
	DEBUG("db_config->netmask=%s", db_config->netmask);
	DEBUG("db_config->dns1=%s", db_config->dns1);
	DEBUG("db_config->dns2=%s", db_config->dns2);
	DEBUG("db_config->pool=%s", db_config->pool);
	DEBUG("-----------------END--------------");			
	
	INFO("Loading...");
		
	//uses db_config to create the database file
	if(create_allocation_base(db_config) < 0) {

        ERROR("***error!***can't create allocation base==>");
        free(db_config);
        return main_error(dhcp_socket);

    }
	
	free(db_config);
	
	DEBUG("Configuration loaded");

	//socket binding
	int so_reuseaddr = 1;
	struct sockaddr_in server_address;

	if((dhcp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
		
		FATAL("***Cannot open the socket! %s(%d)***", strerror(errno), errno);
		return main_error(dhcp_socket);
		
	}

	setsockopt(dhcp_socket, SOL_SOCKET, SO_REUSEADDR, &so_reuseaddr, sizeof(so_reuseaddr));
	
	memset(&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(LISTEN_PORT);
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(dhcp_socket, (struct sockaddr*)&server_address, sizeof(server_address)) < 0){
		
		FATAL("***Cannot bind the socket with the address! %s(%d)***", strerror(errno), errno);
		return main_error(dhcp_socket);
		
	}		
	
	time_t start = time(NULL);
	time_t end;
	
	socklen_t addr_len = sizeof(struct sockaddr_in);
	
	INFO("Server started!");
	
	//infinite loop
	while(1){

        double diff;
		raw_msg *msg = calloc(1, sizeof(*msg));
		
		if(msg == NULL){
			
			FATAL("***Allocate memory failed! %s(%d)***", strerror(errno), errno);
			return main_error(dhcp_socket);
			
		}
		
		msg->socket_fd = dhcp_socket;
		msg->length = (uint32_t)recvfrom(msg->socket_fd, msg->buff, DHCP_MAX_MTU, 0, (struct sockaddr*)&msg->address, &addr_len); //here receives message
		DEBUG("%d bytes received", msg->length);
		
		if(msg->length < 0){
			
			WARN("***Receive data error! %s(%d)***", strerror(errno), errno);
			free(msg);
			continue;
			
		}
		
		end = time(NULL);
		diff = difftime(end, start);      //calculate time difference
		
		update_bindings(diff);            //update of current bindings based on the time difference
		
		handle_msg(msg);                  //process the incoming message
		start = end;                      //reset time counting
		
	}
	
}

/**
* 
* @details The message pointer is deserialized into a dhcp_packet and through dispatch function another dhcp_packet is generated, and this should be sended away.
* @details In order to do this, it binds a broadcast socket to the server address and uses it to send the packet, after it is serialized.
* @details When packet is sent or there's an error, binding is destroyed and structs are freed.
*
*/
void handle_msg(raw_msg *msg){
	
	INFO("==>handle_msg, msg=%d", msg);
	dhcp_packet *request = deserialize(msg->buff, 0, msg->length); //deserializing packet
	
	if(request != NULL){
		
		dhcp_packet *response = dispatch(request);   //dispatch builds response from request
		
        if(response != NULL){              //if server has to send a reply
			
			int broadcast_socket = 0;
    		int so_broadcast = 1;
			int so_reuseaddr = 1;
    		struct sockaddr_in server_address;
            
			//builds socket
    		if((broadcast_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
				
        		FATAL("***Cannot open the socket! %s(%d)***", strerror(errno), errno);			
    			return release_bind(broadcast_socket, response, request, msg);            //error here frees the packets
				
    		}

    		setsockopt(broadcast_socket, SOL_SOCKET, SO_BROADCAST, &so_broadcast, sizeof(so_broadcast));
			setsockopt(broadcast_socket, SOL_SOCKET, SO_REUSEADDR, &so_reuseaddr, sizeof(so_reuseaddr));
		
		    char server_ip[16];
			retrieve_info(CONFIG_SERVER_ADDRESS, server_ip);
		
    		memset(&server_address, 0, sizeof(server_address));
    		server_address.sin_family = AF_INET;
    		server_address.sin_port = htons(LISTEN_PORT);
    		server_address.sin_addr.s_addr = inet_addr(server_ip);
            
			//binds socket
            if(bind(broadcast_socket, (struct sockaddr*)&server_address, sizeof(server_address)) < 0){
				
        		FATAL("***Cannot bind the socket with the address! %s(%d)***", strerror(errno), errno);
        		release_bind(broadcast_socket, response, request, msg);
                return;
				
    		}

            else{
				
			   char buffer[DHCP_MAX_MTU];
			   int length = serialize(response, buffer, DHCP_MAX_MTU);  //serializing packet to be sent
			
			   struct sockaddr_in broadcast = {0};
			   broadcast.sin_family = AF_INET;
			   broadcast.sin_port = htons(REPLY_PORT);
			   broadcast.sin_addr.s_addr = htonl(INADDR_BROADCAST);

			   int send_length = (int)sendto(broadcast_socket, buffer, (size_t)length, 0, (struct sockaddr*)&broadcast, sizeof(broadcast)); //sending the packet to client
			
			   if(send_length < 0)			
				  WARN("***Send data error! %s(%d)***", strerror(errno), errno);
			 
			   else			
				  DEBUG("Total %d bytes sent!", send_length);
			
			   release_bind(broadcast_socket, response, request, msg); //either packet is sent or not, binding is destroyed and packets are freed
			   return;

			}							
			
		}		
			
        else		
		   WARN("Response packet is NULL.");	
			
		free_packet(request);
		
	}
	
	else		
		WARN("Can not unserialize request packet from raw bytes.");
		
	free(msg);	
	INFO("handle_msg==>");
	
}

/**
* 
* @details Checks the function argument, analyzes it and chooses through a switch-case which function to execute in order to generate a response packet
*
*/
dhcp_packet *dispatch(dhcp_packet *request){
	
	INFO("==>dispatch");
	
	if(request== NULL){
		
		ERROR("***Request packet is NULL***");
		return dispatch_error();
		
	}

	if(request->op != BOOT_REQUEST){
		
		WARN("***Packet is not send from dhcp client, ignore!***");
		return dispatch_error();
		
	}

	//gets the dhcp packet type
	char type = '\0';
	dhcp_option *option = request->options;
	
	while(option != NULL)
	{
		if(option->code == DHO_DHCP_MESSAGE_TYPE){
			
			type = *(char*)option->value;
			break;
			
		}
		
		option = option->next;
		
	}

	if(type == '\0'){
		
		ERROR("***No dhcp message type found in the packet!***");
		return dispatch_error();
		
	}
	
	DEBUG("packet type=%d", type);
	dhcp_packet *response = NULL;
	
	//switch-case result depending from the DHCP packet type
	switch(type){
		
		case DHCP_DISCOVER:
			response = do_offer(request);
			break;
		case DHCP_REQUEST:
			response = do_acknowledgement(request);
			break;
		case DHCP_RELEASE:
			response = do_release(request);
			break;				
		case DHCP_DECLINE:
			response = do_decline(request);
			break;
		case DHCP_INFORM:
		    response = do_inform(request);
			break;
		default:
			break;
			
	}
	
	INFO("dispatch==>");
	return response;
	
}

/**
* 
* @details Takes mac address of client, reserves an ip address for it and prepares a list of dhcp_option struct to be sent back (DHCPOFFER packet)
*
*/
dhcp_packet *do_offer(dhcp_packet *request){
	
	INFO("==>do_offer");
	network_config config = {0};
	memcpy(config.hardware_address, request->chaddr, 16);
	
	if(ip_allocator(&config) < 0){ //add mac in database and reserves ip for it, then gets configuration
		
		WARN("Cannot assign IP address! do_offer==>");
		return NULL;
		
	}    	
	
	//dynamic allocation of response
	dhcp_packet *response = calloc(1, sizeof(*response));
	
	if(response == NULL){
		
		FATAL("***Allocate memory failed! %s(%d)*** do_offer==>", strerror(errno), errno);
		return NULL;
		
	}	
    
	//fill response
	response->op = BOOT_REPLY;
	response->htype = request->htype;
	response->hlen = request->hlen;
	response->hops = 1;
	memcpy(response->xid, request->xid, 4);
	memcpy(response->yiaddr, config.ip_address, 4);
	memcpy(response->flags, request->flags, 2);
	memcpy(response->chaddr, request->chaddr, 16);	

    char type[2] = {DHCP_OFFER};
	
	//dynamic allocation of options
	dhcp_option *packet_type = calloc(1, sizeof(dhcp_option));	
	dhcp_option *server_identifier = calloc(1, sizeof(dhcp_option));	
	dhcp_option *lease_time = calloc(1, sizeof(dhcp_option));	
	dhcp_option *renew_time = calloc(1, sizeof(dhcp_option));
	dhcp_option *rebind_time = calloc(1, sizeof(dhcp_option));	
	dhcp_option *router = calloc(1, sizeof(dhcp_option));	
	dhcp_option *subnet_mask = calloc(1, sizeof(dhcp_option));	
	dhcp_option *dns_server = calloc(1, sizeof(dhcp_option));
	
	if(packet_type == NULL || server_identifier == NULL || lease_time == NULL || renew_time == NULL || router == NULL || subnet_mask == NULL || dns_server == NULL ){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_discover==>", strerror(errno), errno);
		return NULL;
		
	}
    
	//dynamic allocation of options values
    packet_type->value = strdup(&type[0]); 
	server_identifier->value = strdup(config.server); 
	lease_time->value = malloc(sizeof(uint32_t)); 
	renew_time->value = malloc(sizeof(uint32_t));
	rebind_time->value = malloc(sizeof(uint32_t));
	router->value = strdup(config.router); 
	subnet_mask->value = strdup(config.netmask); 
	dns_server->value = strdup(strcat(config.dns1, config.dns2)); 
	
	if(packet_type->value == NULL || server_identifier->value == NULL || lease_time->value == NULL || renew_time->value == NULL || router->value == NULL ||
	    subnet_mask->value == NULL || dns_server->value == NULL){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_offer==>", strerror(errno), errno);
		return NULL;
		
	}
	
    *(uint32_t *)lease_time->value = htonl(config.lease);          //htonl changes the endianess to big-endian (aka network byte order) if the server uses little-endian
	*(uint32_t *)renew_time->value = htonl(config.renew);
    *(uint32_t *)rebind_time->value = htonl(config.rebind);
	
	//here every value is already placed in memory, however strings need a reallocation because we have to send a char array of fixed size, decided by the protocol
	//the reallocation affects only the string terminator, no important values are discarded
	//no need to realloc the non-string values (they haven't the string terminator...)

	packet_type->value = realloc(packet_type->value, 1);
	server_identifier->value = realloc(server_identifier->value, 4);
	router->value = realloc(router->value, 4);
	subnet_mask->value = realloc(subnet_mask->value, 4);
	dns_server->value = realloc(dns_server->value, 8);
	
	if(packet_type->value == NULL || server_identifier->value == NULL || router->value == NULL || subnet_mask->value == NULL || dns_server->value == NULL){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_offer==>", strerror(errno), errno);
		return NULL;
		
	}
	
	//fill options with remaining data and enqueue them in response
	
	packet_type->code = DHO_DHCP_MESSAGE_TYPE;
	packet_type->length = 1;
	response->options = packet_type;
    	
    server_identifier->code = DHO_DHCP_SERVER_IDENTIFIER;
    server_identifier->length = 4;
    packet_type->next = server_identifier;

    lease_time->code = DHO_DHCP_LEASE_TIME;	
    lease_time->length = 4;
    server_identifier->next = lease_time;

    renew_time->code = DHO_DHCP_RENEWAL_TIME;	
    renew_time->length = 4;
    lease_time->next = renew_time;
	
	rebind_time->code = DHO_DHCP_REBINDING_TIME;
    rebind_time->length = 4;
    renew_time->next = rebind_time;

    router->code = DHO_ROUTERS;
    router->length = 4;
    rebind_time->next = router;

    subnet_mask->code = DHO_SUBNET;
    subnet_mask->length = 4;
    router->next = subnet_mask;		

    dns_server->code = DHO_DOMAIN_NAME_SERVERS;
    dns_server->length = 8;
    subnet_mask->next = dns_server;

	INFO("do_offer==>");
	return response;
	
}

/**
* 
* @details Takes mac address of client, confirms the binding, verifies that this is the right server and that the requested address is equal to the reserved one.
* @details Then prepares the answer for client (DHCPACK).
*
*/
dhcp_packet *do_acknowledgement(dhcp_packet *request){
	
	INFO("==>do_acknowledgement");
	network_config config = {0};
	memcpy(config.hardware_address, request->chaddr, 16);
	
	if(ip_confirmer(&config) < 0){ //verifies that a mac has been reserved in database and gets configuration
		
		WARN("Cannot assign IP address! do_acknowledgement==>");
		return NULL;
		
	}
	
	//check if this is the right server
	dhcp_option *option = request->options;
	
	while(option != NULL){
		
		if(option->code == DHO_DHCP_SERVER_IDENTIFIER && memcmp(config.server, option->value, 4) != 0){
				
			INFO("This is not the server that has to serve this request");
				
				if(ip_releaser(&config) < 0)		
		            WARN("Cannot release IP address! do_release==>");
				
				return NULL;
				
			}
			
		option = option->next;
				
	}
	
	char requested_address[4] = {0};
		
	//gets requested address	
	if(memcmp(requested_address, request->ciaddr, 4) != 0){
		
		INFO("request->ciaddr is not 0, copy it to request_address");
		memcpy(requested_address, request->ciaddr, 4);
		
	}
	
	else{
		
		INFO("request->ciaddr is 0, get request_address from dhcp option");
		option = request->options;
		
		while(option != NULL){
			
			if(option->code == DHO_DHCP_REQUESTED_ADDRESS && option->length >= 4){
				
				memcpy(requested_address, option->value, 4);
				
			}
			
			option = option->next;
			
		}
		
	}	
	
	char type[2] = {DHCP_ACK};
	
	//requested address == reserved one?
    if(memcmp(config.ip_address, requested_address, 4) != 0 ){
		
		WARN("request_address is not the same as IP assigned, changing packet type to NAK");					
		type[0] = DHCP_NAK;
		
	}
	
	//dynamic allocation of response
	dhcp_packet *response = calloc(1, sizeof(*response));
	
	if(response == NULL){
		
		FATAL("***Allocate memory failed! %s(%d)*** do_acknowledgement==>", strerror(errno), errno);
		return NULL;
		
	}

	//fill response
	response->op = BOOT_REPLY;
	response->htype = request->htype;
	response->hlen = request->hlen;
	response->hops = 1;
	memcpy(response->xid, request->xid, 4);
	memcpy(response->yiaddr, config.ip_address, 4);
	memcpy(response->flags, request->flags, 2);
	memcpy(response->chaddr, request->chaddr, 16);	
	
    //dynamic allocation of options
	dhcp_option *packet_type = calloc(1, sizeof(dhcp_option));	
	dhcp_option *server_identifier = calloc(1, sizeof(dhcp_option));	
	dhcp_option *lease_time = calloc(1, sizeof(dhcp_option));	
	dhcp_option *renew_time = calloc(1, sizeof(dhcp_option));
	dhcp_option *rebind_time = calloc(1, sizeof(dhcp_option));
	dhcp_option *router = calloc(1, sizeof(dhcp_option));	
	dhcp_option *subnet_mask = calloc(1, sizeof(dhcp_option));	
	dhcp_option *dns_server = calloc(1, sizeof(dhcp_option));
	
	if(packet_type == NULL || server_identifier == NULL || lease_time == NULL || renew_time == NULL || router == NULL || subnet_mask == NULL || dns_server == NULL ){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_discover==>", strerror(errno), errno);
		return NULL;
		
	}
	
	//dynamic allocation of options values
    packet_type->value = strdup(&type[0]); 
	server_identifier->value = strdup(config.server); 
	lease_time->value = malloc(sizeof(uint32_t)); 
	renew_time->value = malloc(sizeof(uint32_t));
    rebind_time->value = malloc(sizeof(uint32_t));
	router->value = strdup(config.router); 
	subnet_mask->value = strdup(config.netmask); 
	dns_server->value = strdup(strcat(config.dns1, config.dns2)); 
	
	if(packet_type->value == NULL || server_identifier->value == NULL || lease_time->value == NULL || renew_time->value == NULL || router->value == NULL ||
	    subnet_mask->value == NULL || dns_server->value == NULL){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_offer==>", strerror(errno), errno);
		return NULL;
		
	}
	
    *(uint32_t *)lease_time->value = htonl(config.lease);
	*(uint32_t *)renew_time->value = htonl(config.renew);
	*(uint32_t *)rebind_time->value = htonl(config.rebind);
	
	//see do_offer for realloc explanation
	
	packet_type->value = realloc(packet_type->value, 1);
	server_identifier->value = realloc(server_identifier->value, 4);
	router->value = realloc(router->value, 4);
	subnet_mask->value = realloc(subnet_mask->value, 4);
	dns_server->value = realloc(dns_server->value, 8);
	
	if(packet_type->value == NULL || server_identifier->value == NULL || router->value == NULL || subnet_mask->value == NULL || dns_server->value == NULL){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_offer==>", strerror(errno), errno);
		return NULL;
		
	}

	//fill options with remaining data and enqueue them in response
	
	packet_type->code = DHO_DHCP_MESSAGE_TYPE;
	packet_type->length = 1;
	response->options = packet_type;  		

    server_identifier->code = DHO_DHCP_SERVER_IDENTIFIER;
    server_identifier->length = 4;
    packet_type->next = server_identifier;		

    lease_time->code = DHO_DHCP_LEASE_TIME;
    lease_time->length = 4;
    server_identifier->next = lease_time;

    renew_time->code = DHO_DHCP_RENEWAL_TIME;
    renew_time->length = 4;
    lease_time->next = renew_time;
	
	rebind_time->code = DHO_DHCP_REBINDING_TIME;
    rebind_time->length = 4;
    renew_time->next = rebind_time;

    router->code = DHO_ROUTERS;
    router->length = 4;
    rebind_time->next = router;

    subnet_mask->code = DHO_SUBNET;
    subnet_mask->length = 4;
    router->next = subnet_mask;		

    dns_server->code = DHO_DOMAIN_NAME_SERVERS;
    dns_server->length = 8;
    subnet_mask->next = dns_server;
	
	INFO("do_acknowledgement==>");
	return response;
	
}

/**
* 
* @details Takes mac address of client and releases the binding with it
*
*/
dhcp_packet *do_release(dhcp_packet *request){
	
	INFO("==>do_release");
	
	network_config config = {0};
	memcpy(config.hardware_address, request->chaddr, 16);
	
	if(ip_releaser(&config) < 0)		
		WARN("Cannot release IP address! do_release==>");					
	
	return NULL;
	
	INFO("do_release==>");
	
}

/**
* 
* @details Gets configuration and writes it into a DHCPACK packet.
*
*/
dhcp_packet *do_inform(dhcp_packet *request){
	
	INFO("==>do_inform");
    network_config config = {0};
	
	ip_confirmer(&config);    //mac is 0 so it only gets config (see ip_confirmer code)
		
	//dynamic allocation of response
	dhcp_packet *response = calloc(1, sizeof(*response));
	
	if(response == NULL){
		
		FATAL("***Allocate memory failed! %s(%d)*** do_inform==>", strerror(errno), errno);
		return NULL;
		
	}	
	
	//notice that here yiaddr is not filled, as the protocol wants
	response->op = BOOT_REPLY;
	response->htype = request->htype;
	response->hlen = request->hlen;
	response->hops = 1;
	memcpy(response->xid, request->xid, 4);
	memcpy(response->flags, request->flags, 2);
	memcpy(response->chaddr, request->chaddr, 16);	
			
	char type[2] = {DHCP_ACK};
	
	//notice also that in inform response packet lease isn't sent
	dhcp_option *packet_type = calloc(1, sizeof(dhcp_option));	
	dhcp_option *server_identifier = calloc(1, sizeof(dhcp_option));
	dhcp_option *renew_time = calloc(1, sizeof(dhcp_option));
	dhcp_option *rebind_time = calloc(1, sizeof(dhcp_option));
	dhcp_option *router = calloc(1, sizeof(dhcp_option));	
	dhcp_option *subnet_mask = calloc(1, sizeof(dhcp_option));	
	dhcp_option *dns_server = calloc(1, sizeof(dhcp_option));
	
	if(packet_type == NULL || server_identifier == NULL || renew_time == NULL || router == NULL || subnet_mask == NULL || dns_server == NULL ){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_inform==>", strerror(errno), errno);
		return NULL;
		
	}	
	
	//dynamic allocation of options values
    packet_type->value = strdup(&type[0]); 
	server_identifier->value = strdup(config.server);
	renew_time->value = malloc(sizeof(uint32_t));
	rebind_time->value = malloc(sizeof(uint32_t));
	router->value = strdup(config.router); 
	subnet_mask->value = strdup(config.netmask); 
	dns_server->value = strdup(strcat(config.dns1, config.dns2)); 
	
	if(packet_type->value == NULL || server_identifier->value == NULL || renew_time->value == NULL || router->value == NULL ||
	    subnet_mask->value == NULL || dns_server->value == NULL){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_offer==>", strerror(errno), errno);
		return NULL;
		
	}
	
	*(uint32_t *)renew_time->value = htonl(config.renew);	
	*(uint32_t *)rebind_time->value = htonl(config.rebind);
	
	//see do_offer for realloc explanation
	
	packet_type->value = realloc(packet_type->value, 1);
	server_identifier->value = realloc(server_identifier->value, 4);
	router->value = realloc(router->value, 4);
	subnet_mask->value = realloc(subnet_mask->value, 4);
	dns_server->value = realloc(dns_server->value, 8);
	
	if(packet_type->value == NULL || server_identifier->value == NULL || router->value == NULL || subnet_mask->value == NULL || dns_server->value == NULL){
		
		free_packet(response);
		FATAL("***Allocate memory failed! %s(%d)*** do_offer==>", strerror(errno), errno);
		return NULL;
		
	}
	
	//fill options with remaining data and enqueue them in response
	
	packet_type->code = DHO_DHCP_MESSAGE_TYPE;
	packet_type->length = 1;
	response->options = packet_type;  		

    server_identifier->code = DHO_DHCP_SERVER_IDENTIFIER;
    server_identifier->length = 4;
    packet_type->next = server_identifier;		

    renew_time->code = DHO_DHCP_RENEWAL_TIME;
    renew_time->length = 4;
    server_identifier->next = renew_time;
	
	rebind_time->code = DHO_DHCP_REBINDING_TIME;
    rebind_time->length = 4;
    renew_time->next = rebind_time;

    router->code = DHO_ROUTERS;
    router->length = 4;
    rebind_time->next = router;

    subnet_mask->code = DHO_SUBNET;
    subnet_mask->length = 4;
    router->next = subnet_mask;		

    dns_server->code = DHO_DOMAIN_NAME_SERVERS;
    dns_server->length = 8;
    subnet_mask->next = dns_server;
	
	INFO("do_inform==>");
	return response;
	
}

/**
* 
* @details Takes mac address of client and 'blocks'(see ip_blocker) the row in the database where is placed the mac-ip association.
*
*/
dhcp_packet *do_decline(dhcp_packet *request){
	
	INFO("==>do_decline");
	
	network_config config = {0};
	memcpy(config.hardware_address, request->chaddr, 16);
	
	if(ip_blocker(&config) < 0)		
		WARN("Cannot remove IP address! do_decline==>");		
				
	return NULL;
	
	INFO("do_decline==>");
	
}
