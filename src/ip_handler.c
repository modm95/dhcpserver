/**
* @file ip_handler.c
* @brief Functions for database management
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.17
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <unistd.h>

#include "log.h"
#include "dhcp_server.h"
#include "ip_handler.h"

#define SQLITE_OPEN "open"
#define SQLITE_PREPARE "prepare"
#define SQLITE_STEP "step"
#define SQLITE_EXECUTE "execute"

extern char db_name[256];

/**
* 
* @brief Called if there's an error while deserializing
*
* @details Warns about error, closes the database and returns
*
* @param sql_error_location A string containing which SQL function caused the error
* @param db A pointer to an opened database file
* @param ret The value returned by the SQL function that caused the error
*
* @returns -1
* 
*/
int send_err_sqlite(char* sql_error_location, sqlite3 *db, int ret){
	
	WARN("***ERROR sqlite==>***");
	ERROR("***sqlite3 %s ERROR!!! %s(%d)***", sql_error_location, sqlite3_errmsg(db), ret);
   
	sqlite3_close(db);
			      	
	return -1;
	
}

/**
* 
* @details The function starts a loop where an uint64 variable is multiplied by 0x100 and added 8 bits of the mac address.
* @details This loop is performed six times, starting with a uint64 variable that has value 0, and ending with the same variable that has the mac address converted in integer form.
*
*/
uint64_t get_uint_mac(network_config *config){

	uint64_t mac = 0x0000000000000000;

	for(int i = 0; i < 6; i++){

		mac *= 0x100;                                        //multiply by 0x100
		mac += (uint8_t)config->hardware_address[i];         //add 8 bits and repeat

	}

	DEBUG("mac address=%02x:%02x:%02x:%02x:%02x:%02x, integer value=%ld", (uint8_t)config->hardware_address[0], (uint8_t)config->hardware_address[1],
		  (uint8_t)config->hardware_address[2], (uint8_t)config->hardware_address[3], (uint8_t)config->hardware_address[4], (uint8_t)config->hardware_address[5], mac);

	return mac;

}


/**
* 
* @details Checks if ip_address string is valid, then takes the integers from ip_address and stores them into a string composed by 4 'bytes'
* 
*/
void ip_atob(char bytes[5], const char *ip_address){
	
	INFO("==>ip_atob, ip_address=%s", ip_address);
	
    if(ip_address == NULL || strlen(ip_address) > 15 || strlen(ip_address) < 7){
		
        ERROR("***IP address is invalid, ip_atob==>***");
        return;
		
    }
	
    unsigned short a, b, c, d;
    sscanf(ip_address, "%hu.%hu.%hu.%hu", &a, &b, &c, &d);  //uses ipv4 format for taking the integers
	
	//and stores them individually
	bytes[0] = a;
	bytes[1] = b;
	bytes[2] = c;
	bytes[3] = d;
	bytes[4] = '\0';
	
	INFO("ip_atob==>");	
	
}


/**
*
* @details Takes a pool and parses it, verifies that is a valid range of private ip addresses and goes on collecting the boundaries in the form of two strings
*
*/	
pool_info *parse_pool(char pool[32]){
	
	int a, b, c, d, e, f, g, h = 0;
	sscanf(pool, "%d.%d.%d.%d-%d.%d.%d.%d", &a, &b, &c, &d, &e, &f, &g, &h);  //stores ip int values into variables
	
	switch(a){
		
		case 10:
		
			if(a != e)
			   return NULL;
		   
			break;
			
		case 172:
		
			if(a != e || b < 16 || f > 32)
   		        return NULL;	
		
			break;
			
		case 192:
		
			if(a != e || b != 168 || b!= f)
				return NULL;
				
			break;
			
		default:		
			return NULL;
			
	}	
	
	pool_info *pool_config_info = calloc(1, sizeof(*pool_config_info));	 //allocs space in memory

	//uses variables to store info in the struct
    snprintf(pool_config_info->first, 16, "%d.%d.%d.%d", a, b, c, d);
	snprintf(pool_config_info->last, 16, "%d.%d.%d.%d", e, f, g, h);
	
	return pool_config_info;     //returns the struct pointer
	
}
	

/**
*
* @details Builds a database file from scratch, dropping everything that already exist
* @details Creates two tables, one for mac-ip associations management and one for storing network parameters
* @details Then fills the second one with the network parameters and the ip column of the first one with all the ips in pool range
*
*/
int create_allocation_base(net_temp_config *db_config){
		
	sqlite3 *db = NULL;				
	
	//creates the database
	const char *sql = "DROP TABLE IF EXISTS network_config;"
                      "DROP TABLE IF EXISTS mac_ip;"
			          "CREATE TABLE network_config(server text, lease_time int, renew_time int, rebind_time int, gateway text, netmask text, dns1 text, dns2 text);"
	                  "CREATE TABLE mac_ip(mac bigint, ip text, lease double);";
	
	int ret = sqlite3_open(db_name, &db);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_OPEN, db, ret);
	
	ret = sqlite3_exec(db, sql, 0, 0, NULL);	
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_EXECUTE, db, ret);
	
	//fills it with the needed config
	char insert[256] = {0};
	snprintf(insert, 256, "INSERT INTO network_config VALUES('%s', '%d', '%d', '%d', '%s', '%s', '%s', '%s');", db_config->server, db_config->lease, db_config->renew,
			 db_config->rebind, db_config->gateway, db_config->netmask, db_config->dns1, db_config->dns2);
	
	ret = sqlite3_exec(db, insert, 0, 0, NULL);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_EXECUTE, db, ret);
	
	//and uses pool info to insert all the ips
	pool_info *pool_config = parse_pool(db_config->pool);

    if(pool_config == NULL){

        ERROR("**Error in pool parsing->pool format not correct!**");
        return -1;

    }	
	
    char insert_ip[128] = {0};
	int a, b, c, d = 0;
    sscanf(pool_config->first, "%d.%d.%d.%d", &a, &b, &c, &d);
	
    DEBUG("Inserting %s", pool_config->first);
	
	while(strncmp(pool_config->first, pool_config->last, 16) != 0){
		 
	    snprintf(insert_ip, 128, "INSERT INTO mac_ip(ip, lease) VALUES('%s', 0);", pool_config->first); //inserts ip
		
		ret = sqlite3_exec(db, insert_ip, 0, 0, NULL);
	
	    if(ret != SQLITE_OK)
		    return send_err_sqlite(SQLITE_EXECUTE, db, ret);
		
		//updates the ip
		d++;  
		
		if(d > 255){
			
			d = 0;			
			c++;
			
			if (c > 255){
				
				c = 0;
				b++;
				
			}
			
		    //a is not updated because no class of ipv4 private addresses has a range that updates this field
			
		}
		
		//now the new ip is memorized
		snprintf(pool_config->first, 16, "%d.%d.%d.%d", a, b, c, d);
		DEBUG("Inserting %s", pool_config->first);
		
		//ips will be updated and inserted until first = last
		
	}
	
	snprintf(insert_ip, 128, "INSERT INTO mac_ip(ip, lease) VALUES('%s', 0);", pool_config->last); //inserts last ip
	
	ret = sqlite3_exec(db, insert_ip, 0, 0, NULL);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_EXECUTE, db, ret);	
			
	free(pool_config);
    sqlite3_close(db);
	return 0;
	
}

/**
*
* @details Gets a single field from network_config table in database
*
*/
int retrieve_info(char *column, char value[16]){
	
	sqlite3 *db = NULL;
	sqlite3_stmt *statement = NULL;
	char *types[] = {CONFIG_SERVER_ADDRESS, CONFIG_LEASE_TIME, CONFIG_RENEW_TIME, CONFIG_REBIND_TIME, CONFIG_GATEWAY, CONFIG_NETMASK, CONFIG_DNS1, CONFIG_DNS2};
	
	int ret = sqlite3_open(db_name, &db);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_OPEN, db, ret);
    
    ret = sqlite3_prepare_v2(db, "SELECT * FROM network_config LIMIT 1;", 128, &statement, NULL);

    if(ret != SQLITE_OK)
       return send_err_sqlite(SQLITE_PREPARE, db, ret);
				    
    ret = sqlite3_step(statement);

    if(ret != SQLITE_ROW)
       return send_err_sqlite(SQLITE_STEP, db, ret);	
	
	memset(value, 0, 16);

	//here it searches which is the right column, when it's found it decides if it has to retrieve an int or a string
	//lease time and renew time are stored as int, the other parameters are strings
	for(int i = 0; i < 8; i++){

		if (strcmp(column, types[i]) == 0) {

			if (strcmp(column, CONFIG_LEASE_TIME) == 0 || strcmp(column, CONFIG_RENEW_TIME) == 0 || strcmp(column, CONFIG_REBIND_TIME) == 0)
				snprintf(value, 16, "%d", sqlite3_column_int(statement, i));

			else
				snprintf(value, 16, "%s", sqlite3_column_text(statement, i));

		}

	}

	sqlite3_finalize(statement);	
    sqlite3_close(db);
	
	return 0;
	
}

/**
*
* @details Retrieves the configuration stored in the database and an ip address associated to the mac address of a client
* @details The mac address is inside config (the function parameter) and all the retrieved info is memorized inside config
*
*/
int ip_allocator(network_config *config){
	
	INFO("==>ip_allocator");	
	
	//takes the mac address of the client
	uint64_t mac = get_uint_mac(config);
    
	sqlite3 *db = NULL;
	sqlite3_stmt *statement = NULL;
	
	int ret = sqlite3_open(db_name, &db);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_OPEN, db, ret);
		
	//inserts the mac in the database, adds 20 seconds to lease (gives enough time to client to send the request without having his mac deleted from the server)
	char mac_sql[128] = {0};
	snprintf(mac_sql, 128, "UPDATE mac_ip SET mac = '%lu', lease = 20 WHERE mac IS NULL LIMIT 1", mac);
	   
	ret = sqlite3_exec(db, mac_sql, 0, 0, NULL);	   
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_EXECUTE, db, ret);
	   	   
	snprintf(mac_sql, 128, "SELECT * FROM mac_ip WHERE mac IS '%lu' LIMIT 1", mac);
	ret = sqlite3_prepare_v2(db, mac_sql, 128, &statement, NULL);
	   
	if(ret != SQLITE_OK)       	
		return send_err_sqlite(SQLITE_PREPARE, db, ret);	
	   
	ret = sqlite3_step(statement);
	   
	if(ret != SQLITE_ROW)
		return send_err_sqlite(SQLITE_STEP, db, ret);
	
	//takes the ip in that row	
	const char *result = (const char *)sqlite3_column_text(statement, 1);			
    ip_atob(config->ip_address, result);

	sqlite3_finalize(statement);
    sqlite3_close(db);
	
	char value[16];
	
	//configuration and ip address are converted in 'bytes' if needed and memorized in config struct
	
	retrieve_info(CONFIG_SERVER_ADDRESS, value);
	ip_atob(config->server, value);
	
	retrieve_info(CONFIG_LEASE_TIME, value);
    config->lease = (uint32_t)strtol(value, NULL, 0);
	
	retrieve_info(CONFIG_RENEW_TIME, value);
    config->renew = (uint32_t)strtol(value, NULL, 0);
	
	retrieve_info(CONFIG_REBIND_TIME, value);
    config->rebind = (uint32_t)strtol(value, NULL, 0);

    retrieve_info(CONFIG_GATEWAY, value);
	ip_atob(config->router, value);
	
	retrieve_info(CONFIG_NETMASK, value);
    ip_atob(config->netmask, value);
	
	retrieve_info(CONFIG_DNS1, value);
    ip_atob(config->dns1, value);
	
	retrieve_info(CONFIG_DNS2, value);
    ip_atob(config->dns2, value);

	INFO("ip_allocator==>");
	return 0;	
	
}

/**
*
* @details If MAC is not empty (reply to DHCPREQUEST), finds the mac inserted from ip_allocator, starts lease and retrieves the IP and the configuration
* @details Otherwise (reply to DHCPINFORM), the functions only retrieves the configuration (lease not included)
* @details The mac address is inside config (the function parameter) and all the retrieved info is memorized inside config
*
*/
int ip_confirmer(network_config *config){
	
	INFO("==>ip_confirmer");
	
	//takes the mac address of the client
	uint64_t mac = get_uint_mac(config);
	char value[16];
	
	if(mac != 0){
    
		sqlite3 *db = NULL;
		sqlite3_stmt *statement = NULL;
	
		int ret = sqlite3_open(db_name, &db);
	
		if(ret != SQLITE_OK)
			return send_err_sqlite(SQLITE_OPEN, db, ret);
	
		//finds the mac inside the database
		
		char mac_sql[128] = {0};
		snprintf(mac_sql, 128, "SELECT * FROM mac_ip WHERE mac IS %lu LIMIT 1", mac);
	
		ret = sqlite3_prepare_v2(db, mac_sql, 128, &statement, NULL);

		if(ret != SQLITE_OK)       	
			return send_err_sqlite(SQLITE_PREPARE, db, ret);	   
	
		ret = sqlite3_step(statement);

		if(ret != SQLITE_ROW){
	   	
			WARN("**Client mac is not in the database->someone else is allocating it!**");
			sqlite3_close(db);
			return -1;
	   
		}
				
		//takes the ip in that row	
		//configuration and ip address are converted in bytes and memorized in config struct
		const char* result = (const char *)sqlite3_column_text(statement, 1);		
		ip_atob(config->ip_address, result);
	
		sqlite3_finalize(statement);
	
		retrieve_info(CONFIG_LEASE_TIME, value);
		config->lease = (uint32_t)strtol(value, NULL, 0);
	
		//sets lease time for the ip
		//adds 5 secs to lease to avoid wrong autoreleases caused by not perfect time sync between client and server
		snprintf(mac_sql, 128, "UPDATE mac_ip SET lease = '%f' + 5  WHERE mac IS %lu LIMIT 1", (double)strtol(value, NULL, 0), mac);
	
		ret = sqlite3_exec(db, mac_sql, 0, 0, NULL);	   
	
		if(ret != SQLITE_OK)
			return send_err_sqlite(SQLITE_EXECUTE, db, ret);
		
		sqlite3_close(db);
	
	}
	
	retrieve_info(CONFIG_SERVER_ADDRESS, value);
	ip_atob(config->server, value);
	
	retrieve_info(CONFIG_RENEW_TIME, value);
    config->renew = (uint32_t)strtol(value, NULL, 0);
	
	retrieve_info(CONFIG_REBIND_TIME, value);
    config->rebind = (uint32_t)strtol(value, NULL, 0);

    retrieve_info(CONFIG_GATEWAY, value);
	ip_atob(config->router, value);

	retrieve_info(CONFIG_NETMASK, value);
    ip_atob(config->netmask, value);

	retrieve_info(CONFIG_DNS1, value);
    ip_atob(config->dns1, value);
	
	retrieve_info(CONFIG_DNS2, value);
    ip_atob(config->dns2, value);

	INFO("ip_confirmer==>");
	return 0;

}
/**
*
* @details Finds the client mac in the database and deletes it and its lease, without deleting the ip (meaning that the ip is ready to be reallocated)
*
*/
int ip_releaser(network_config *config){
	
	INFO("ip_releaser==>");
	
	sqlite3 *db = NULL;
	int ret = sqlite3_open(db_name, &db);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_OPEN, db, ret);
	
    //takes the mac address
	uint64_t mac = get_uint_mac(config);
	
	//finds the mac inside the database, wipes it and his lease from its line
	char mac_sql[128] = {0};
	snprintf(mac_sql, 128, "UPDATE mac_ip SET mac = NULL, lease = 0 WHERE mac IS '%lu' LIMIT 1", mac);
	
	ret = sqlite3_exec(db, mac_sql, 0, 0, NULL);
	
	   if(ret != SQLITE_OK)
		   return send_err_sqlite(SQLITE_EXECUTE, db, ret);
	    
	sqlite3_close(db);    
	
	INFO("ip_releaser==>");
	return 0;
	
}

/**
*
* @details Sets a mac address to -1 (meaning that the ip in that line can't be reused by this server, until that -1 will be erased)
* @details Sets also a "lease" for this ip, named reset_time
*
*/
int ip_blocker(network_config *config){
	
	INFO("==>ip_blocker");
	
	sqlite3 *db = NULL;
	int ret = sqlite3_open(db_name, &db);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_OPEN, db, ret);
	
	//takes the mac address
	uint64_t mac = get_uint_mac(config);

    char value[16];
    retrieve_info(CONFIG_LEASE_TIME, value);

    //this is part of this server policy: reset_time is a function of lease time, function defined by the author in this line
    double reset_time = round(((int)strtol(value, NULL, 0))/((1/12)*(((int)strtol(value, NULL, 0))/3600)+4));

	//finds the mac inside the database and changes its value to -1
	char mac_sql[128] = {0};
	snprintf(mac_sql, 128, "UPDATE mac_ip SET mac = -1, lease = '%f' WHERE mac IS '%lu' LIMIT 1", reset_time, mac);
	
	ret = sqlite3_exec(db, mac_sql, 0, 0, NULL);
	
	   if(ret != SQLITE_OK)
		   return send_err_sqlite(SQLITE_EXECUTE, db, ret);
	    
	sqlite3_close(db);   
	
	INFO("ip_blocker==>");
	return 0;				
	
}

/**
*
* @details Updates lease using diff and releases bindings that have lease <=0
*
*/
int update_bindings(double diff){
	
	INFO("==>update_bindings");
	
	sqlite3 *db = NULL;
	int ret = sqlite3_open(db_name, &db);
	
	if(ret != SQLITE_OK)
		return send_err_sqlite(SQLITE_OPEN, db, ret);
	
	//for every mac-ip association, subtract its lease by diff
	char lease_sql[128] = {0};
	snprintf(lease_sql, 128, "UPDATE mac_ip SET lease = lease - '%f' WHERE lease > 0", diff);
	
	ret = sqlite3_exec(db, lease_sql, 0, 0, NULL);
	
	   if(ret != SQLITE_OK)
		   return send_err_sqlite(SQLITE_EXECUTE, db, ret);
	
    //release the mac if its lease is <= 0	
	char *free_sql = "UPDATE mac_ip SET mac = NULL, lease = 0 WHERE lease <= 0";
	
	ret = sqlite3_exec(db, free_sql, 0, 0, NULL);
	
	   if(ret != SQLITE_OK)
		   return send_err_sqlite(SQLITE_EXECUTE, db, ret);
	      
	sqlite3_close(db);   
	
	INFO("update_bindings==>");
	return 0;				
	
}

