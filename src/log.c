/**
* @file log.c
* @brief Functions providing log features
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.16
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>

#include "log.h"

/* Default log configuration */
log_config global_file_config = {
	
    .log_enabled = 0,
    .log_level = 1,
    .log_file_dir = "log",
		
};

/**
*
* @details Opens the config_file file and parses it, retrieving all the log configuration parameters set by the user, line by line, and placing them into the global_file_config struct.
* @details Then closes the file and creates a folder where logs should be written (if it doesn't exist).
*
*/
int log_init(char *config_file){
	
	FILE *file = fopen(config_file, "r");  //opens file
	
	if(file == NULL)
	   return -1;
				
	char buffer[CONFIG_BUFFER_SIZE];

	while(!feof(file)){
		
		if(fgets(buffer, CONFIG_BUFFER_SIZE, file) != NULL){	
		
			int index = 0;
			
			for(; buffer[index] != '='; index++);		//goes on in a line and stops when there's '='			
			
			buffer[index] = '\0';
			char *value_begin = buffer + index + 1;
			int value_length = (int)strlen(value_begin);
			
			//distinguish the various parameters to be read after the '=' and store them in the config struct
			
			if(strcmp(buffer, CONFIG_LOG_ENABLED) == 0){
				
				memcpy(&global_file_config.log_enabled, value_begin, 1);
				global_file_config.log_enabled = global_file_config.log_enabled - '0';
				
			}
			
			else if(strcmp(buffer, CONFIG_LOG_LEVEL) == 0){
				
				memcpy(&global_file_config.log_level, value_begin, 1);
				global_file_config.log_level = global_file_config.log_level - '0';
				
			}
			
			else if(strcmp(buffer, CONFIG_LOG_FILE_DIR) == 0){
				
				if(value_begin[value_length - 1] == '\n')					
				   value_begin[value_length - 1] = '\0';									

				strncpy(global_file_config.log_file_dir, value_begin , MAX_FILE_PATH);
				
			}
			
		}	
		
	}
	
	fclose(file);

    mkdir(global_file_config.log_file_dir, 0777);     //if folder for log storing doesn't exist, create it

	return 0;
	
}

/**
*
* @details First checks if log has been enabled by user. If so, the function creates a log file (if it doesn't exist) and opens it.
* @details After that, writes the message in the file, along with detailed info about the message, such as date and gid/pid
* @details Finally, after closing the file, writes the message on screen, but only if verbosity set by user allowes it.
*
*/
void write_log(char level, char *message, ...){

	if(global_file_config.log_enabled == 0)	//check if log is enabled
		return;		
	
	time_t now;  //gets date info
	time(&now);
	struct tm *tm_now = localtime(&now); 
	char file_path[MAX_FILE_PATH] = {0};
	
	//creates the file and opens it
	snprintf(file_path, MAX_FILE_PATH, "%s/%s_%4d-%02d-%02d.log", global_file_config.log_file_dir, LOG_FILE_NAME_PREFIX, tm_now->tm_year + 1900, tm_now->tm_mon + 1, tm_now->tm_mday);		
	FILE *log_file = fopen(file_path, "a+");
	
	if(log_file == NULL)
	   return;
		
	va_list arg_list;
	char buffer[LOG_BUFFER_SIZE];
	
	va_start(arg_list, message);                                 //message to buffer
	vsnprintf(buffer, LOG_BUFFER_SIZE, message, arg_list);
	va_end(arg_list);

	//prints buffer to file along with extra information
	fprintf(log_file, "%4d-%02d-%02d %02d:%02d:%02d\t%05d:%05d  %5s  %s\n", tm_now->tm_year + 1900, tm_now->tm_mon + 1, tm_now->tm_mday, tm_now->tm_hour, tm_now->tm_min,
	 tm_now->tm_sec, getgid(), getpid(), log_level_string(level), buffer);
	 
	fclose(log_file);
	
	//writes buffer to screen if allowed
    if(global_file_config.log_level > 0)
	   if((global_file_config.log_level == 1 && level == LOG_INFO) || (global_file_config.log_level == 2 && (level == LOG_INFO || level == LOG_WARN || level == LOG_ERROR || level == LOG_FATAL))
		  || global_file_config.log_level >= 3)
	       printf("%s  %s\n", log_level_string(level), buffer);
	
}

/**
*
* @details It's a simple switch-case function in which for a certain log_level, an appropriate log_level_string is returned
*
*/
char * log_level_string(char log_level){
	
    switch(log_level){
			
        case LOG_INFO:
            return LOG_INFO_STRING;              
        case LOG_WARN:
            return LOG_WARN_STRING;
        case LOG_ERROR:
            return LOG_ERROR_STRING;
        case LOG_FATAL:
            return LOG_FATAL_STRING;
	    case LOG_DEBUG:
            return LOG_DEBUG_STRING;
        default:
            break;
				
    }

    return NULL;
		
}
