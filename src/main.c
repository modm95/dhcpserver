/**
* @file main.c
* @brief Main function
*
* @author Marco Dalla Mutta <marco.dallamutta@studenti.unipd.it>
* @version 1.00
* @since 0.26
*
* @copyright Copyright (c) 2017
* @copyright MIT License
*
*/

#include<errno.h>
#include<string.h>

#include "dhcp_server.h"
#include "log.h"

int main(void){
	
	//reads log config, terminates the program on error
	if(log_init("conf/dhcp_log.conf")) {  

        FATAL("***Error in log config!***", strerror(errno), errno);
        return -1;
		
    }

	else  //if no error occurred, starts the server using the server config
		return start_server("conf/dhcp_server.conf");   
	
}