# First of all: what's this?

# Disclaimer

This project was made for Laboratorio di Ingegneria Informatica (Computer Science Lab) course exam in University of Padova (Italy), by Marco Dalla Mutta, student.
For report and slides (in italian) see xtra folder.

# This is a...?

DHCP Server IPv4, completely in C.
The program receives DHCP packets from clients and sends replies, based on the request and on a database file that the server uses for keeping track of mac-ip addresses associations.

Tested on Windows 10 and Ubuntu Linux 17.10 , both 64 bit.

## Software used

On Linux, gedit for writing code and configuration.
On Windows, Notepad++ for writing base code and configuration files, and the CLion IDE combined with gdb for optimization and debugging.
Independently from OS, CMake + (g)cc for proper building, Check for unit testing, SQLite3 library (developer edition) for database management, Doxygen for documentation, Git for version control.
Everything on Windows (but CLion and Notepad++ of course) was obtained through Cygwin.

## Getting Started

Needed: a pc, some files from this repo, root account in Linux, administrator account in Windows for Firewall permissions.

What do you have to take from this repo?

* Standard user: 'out' folder
* Developer: Everything ('out' folder not strictly necessary, follow its structure to execute the program)

### Prerequisites for developers

* Windows: Cygwin, and through this CMake, gcc, Check, SQLite3 developer lib (libsqlite3-dev). Binaries will depend on cygwin1.dll, cygsqlite3-0.dll and cygcheck-0.dll.
  
* Linux: Same as Windows but without Cygwin, of course, and with dependence to libsqlite3.so (already present in Linux) and libsubunit.so (only for test files).

Commands for apt:

	sudo apt-get install cmake
	sudo apt-get install check
	sudo apt-get install libsqlite3-dev
	
### Building

Simply execute this in terminal in the top folder of the project to obtain the program from source

    cmake .
    make
	
If you want to perform unit testing execute this

    make test

The commands output should be something like this:

    $ cmake .
    -- The C compiler identification is ...
    -- Check for working C compiler: /usr/bin/cc
    -- Check for working C compiler: /usr/bin/cc -- works
    -- Detecting C compiler ABI info
    -- Detecting C compiler ABI info - done
    -- Detecting C compile features
    -- Detecting C compile features - done
    -- Configuring done
    -- Generating done
    -- Build files have been written to: ...       //folder where you execute the command

    $ make
    Scanning dependencies of target dhcp
	[  6%] Building C object src/CMakeFiles/dhcp.dir/dhcp_server.c.o
	[ 12%] Building C object src/CMakeFiles/dhcp.dir/log.c.o
	[ 18%] Building C object src/CMakeFiles/dhcp.dir/dhcp_packet.c.o
	[ 25%] Building C object src/CMakeFiles/dhcp.dir/ip_handler.c.o
	[ 31%] Linking C static library libdhcp.a
	[ 31%] Built target dhcp
	Scanning dependencies of target dhcpserver
	[ 37%] Building C object src/CMakeFiles/dhcpserver.dir/main.c.o
	[ 43%] Linking C executable ../out/dhcpserver.exe
	[ 43%] Built target dhcpserver
	Scanning dependencies of target check_db
	[ 50%] Building C object tests/CMakeFiles/check_db.dir/check_main.c.o
	[ 56%] Building C object tests/CMakeFiles/check_db.dir/check_db.c.o
	[ 62%] Linking C executable check_db.exe
	[ 62%] Built target check_db
	Scanning dependencies of target check_pkt
	[ 68%] Building C object tests/CMakeFiles/check_pkt.dir/check_main.c.o
	[ 75%] Building C object tests/CMakeFiles/check_pkt.dir/check_pkt.c.o
	[ 81%] Linking C executable check_pkt.exe
	[ 81%] Built target check_pkt
	Scanning dependencies of target check_pktbuild
	[ 87%] Building C object tests/CMakeFiles/check_pktbuild.dir/check_main.c.o
	[ 93%] Building C object tests/CMakeFiles/check_pktbuild.dir/check_pktbuild.c.o
	[100%] Linking C executable check_pktbuild.exe
	[100%] Built target check_pktbuild
	
	$ make test
	Running tests...
    Test project ...           //test folder
        Start 1: check_db
    1/3 Test #1: check_db .........................   Passed    4.48 sec
        Start 2: check_pkt
    2/3 Test #2: check_pkt ........................   Passed    0.95 sec
        Start 3: check_pktbuild
    3/3 Test #3: check_pktbuild ...................   Passed    2.66 sec

    100% tests passed, 0 tests failed out of 3

    Total Test time (real) =   8.10 sec

The program binary, dhcpserver(.exe in Windows), will be generated in the out folder, so cd to that folder to use it.

# Executing

Starting the server is kinda simple, once you set the DHCP server parameters.
For that you need to edit the dhcp_server.conf file. 

    //example
	//all fields are required and have to be inserted with this syntax
    server=192.168.1.6
    lease_time=86400                //time in seconds
    renew_time=43200
    rebind_time=75600
    ip_database_file=ip_database.db   //name you want for the ip database
    gateway=192.168.1.1
    netmask=255.255.255.0
    dns1=8.8.8.8                      //if you don't want to use two dns servers, but only one, write it in both fields
    dns2=8.8.4.4
    pool=192.168.1.7-192.168.1.24   //range of ips, in the format first_ip-last_ip

And also the dhcp_log.conf

    log_enabled=1   //enable with 1, disable with 0
    log_level=0     //level of verbosity (affects only terminal output)
    log_file=log    //folder where to store logs (created automatically if doesn't exist)

For verbosity, numbers <= 0 disable terminal output, 1 outputs only INFO messages, 2 displays also the errors, >= 3 display everything, DEBUG info included.
Configuration MUST be done to ensure the server functionality and must be in a folder named "conf" placed in the same folder of the executable.
Furthermore, the two configuration files need to end with an empty line in order to avoid errors.
WATCH OUT, ensure that the configuration files are ending with an empty line.

Everything set? Let's start the program!
In Windows you can start the exe simply double-clicking on it, but you can also use CmdPrompt or Powershell (give to Firewall the permissions the first time)

    > ./dhcpserver //Powershell
	
	> dhcpserver   //cmd	

In Linux you can execute it using the same syntax of Powershell or, for gaining root privileges:

    $ sudo ./dhcpserver
	
Tests are executable also without using 'make test' and without using developer instruments, but while in Windows the shared libraries take care of everything, in Linux 
the libsubunit.so library used by Check is not present if Check is not installed. The solution is execute from the tests folder

	bash test.sh (uses sudo internally)

to copy the library to the library path and execute the tests.

## Examples

Here's a start test with minimal verbosity (0)

    INFO ==>start_server, config_file=conf/dhcp_server.conf
    INFO  Loading...
    INFO  Server started!

And here's one with maximum verbosity (3)
    
    INFO  ==>start_server, config_file=conf/dhcp_server.conf
    DEBUG  read line from config file: server=192.168.1.6
    DEBUG  read line from config file: lease_time=86400
	DEBUG  read line from config file: renew_time=43200
	DEBUG  read line from config file: rebind_time=75600
	DEBUG  read line from config file: ip_allocator_file=table.db
	DEBUG  read line from config file: gateway=192.168.1.1
	DEBUG  read line from config file: netmask=255.255.255.0
	DEBUG  read line from config file: dns1=8.8.8.8
	DEBUG  read line from config file: dns2=8.8.4.4
	DEBUG  read line from config file: pool=192.168.1.7-192.168.1.24
	DEBUG  -------DUMP CONFIGURATION----------
	DEBUG  db_name=table.db
	DEBUG  db_config->server=192.168.1.6
	DEBUG  db_config->lease=86400
	DEBUG  db_config->renew=43200
	DEBUG  db_config->rebind=75600
	DEBUG  db_config->gateway=192.168.1.1
	DEBUG  db_config->netmask=255.255.255.0
	DEBUG  db_config->dns1=8.8.8.8
	DEBUG  db_config->dns2=8.8.4.4
	DEBUG  db_config->pool=192.168.1.7-192.168.1.24
	DEBUG  -----------------END--------------
	INFO  Loading...
	DEBUG  Inserting 192.168.1.7
	DEBUG  Inserting 192.168.1.8
	DEBUG  Inserting 192.168.1.9
	DEBUG  Inserting 192.168.1.10
	DEBUG  Inserting 192.168.1.11
	DEBUG  Inserting 192.168.1.12
	DEBUG  Inserting 192.168.1.13
	DEBUG  Inserting 192.168.1.14
	DEBUG  Inserting 192.168.1.15
	DEBUG  Inserting 192.168.1.16
	DEBUG  Inserting 192.168.1.17
	DEBUG  Inserting 192.168.1.18
	DEBUG  Inserting 192.168.1.19
	DEBUG  Inserting 192.168.1.20
	DEBUG  Inserting 192.168.1.21
	DEBUG  Inserting 192.168.1.22
	DEBUG  Inserting 192.168.1.23
	DEBUG  Inserting 192.168.1.24
	DEBUG  Configuration loaded
	INFO  Server started!
	
In both cases here's the content of the log file

	2017-12-17 22:53:22	197121:02252   INFO  ==>start_server, config_file=conf/dhcp_server.conf
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: server=192.168.1.6
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: lease_time=86400
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: renew_time=43200
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: rebind_time=75600
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: ip_allocator_file=table.db
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: gateway=192.168.1.1
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: netmask=255.255.255.0
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: dns1=8.8.8.8
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: dns2=8.8.4.4
	2017-12-17 22:53:22	197121:02252  DEBUG  read line from config file: pool=192.168.1.7-192.168.1.24
	2017-12-17 22:53:22	197121:02252  DEBUG  -------DUMP CONFIGURATION----------
	2017-12-17 22:53:22	197121:02252  DEBUG  db_name=table.db
	2017-12-17 22:53:22	197121:02252  DEBUG  db_config->server=192.168.1.6
	2017-12-17 22:53:22	197121:02252  DEBUG  db_config->lease=86400
	2017-12-17 22:53:23	197121:02252  DEBUG  db_config->renew=43200
	2017-12-17 22:53:23	197121:02252  DEBUG  db_config->rebind=75600	
	2017-12-17 22:53:23	197121:02252  DEBUG  db_config->gateway=192.168.1.1
	2017-12-17 22:53:23	197121:02252  DEBUG  db_config->netmask=255.255.255.0
	2017-12-17 22:53:23	197121:02252  DEBUG  db_config->dns1=8.8.8.8
	2017-12-17 22:53:23	197121:02252  DEBUG  db_config->dns2=8.8.4.4
	2017-12-17 22:53:23	197121:02252  DEBUG  db_config->pool=192.168.1.7-192.168.1.24
	2017-12-17 22:53:23	197121:02252  DEBUG  -----------------END--------------
	2017-12-17 22:53:23	197121:02252   INFO  Loading...
	2017-12-17 22:53:23	197121:02252  DEBUG  Inserting 192.168.1.7
	2017-12-17 22:53:23	197121:02252  DEBUG  Inserting 192.168.1.8
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.9
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.10
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.11
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.12
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.13
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.14
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.15
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.16
	2017-12-17 22:53:24	197121:02252  DEBUG  Inserting 192.168.1.17
	2017-12-17 22:53:25	197121:02252  DEBUG  Inserting 192.168.1.18
	2017-12-17 22:53:25	197121:02252  DEBUG  Inserting 192.168.1.19
	2017-12-17 22:53:25	197121:02252  DEBUG  Inserting 192.168.1.20
	2017-12-17 22:53:25	197121:02252  DEBUG  Inserting 192.168.1.21
	2017-12-17 22:53:25	197121:02252  DEBUG  Inserting 192.168.1.22
	2017-12-17 22:53:25	197121:02252  DEBUG  Inserting 192.168.1.23
	2017-12-17 22:53:25	197121:02252  DEBUG  Inserting 192.168.1.24
	2017-12-17 22:53:25	197121:02252  DEBUG  Configuration loaded
	2017-12-17 22:53:25	197121:02252   INFO  Server started!
	
Finally, an example of received of received and handled DHCPDISCOVER (minimum verbosity)

    INFO  Server started!
	INFO  ==>handle_msg, arg=550352
	INFO  ==>deserialize, offset=0, length=244         //'decode' packet
	INFO  dhcp option end
	INFO  deserialize==>
	INFO  ==>dispatch
	INFO  ==>do_offer                                  //reply to DHCPDISCOVER with DHCPOFFER
	INFO  ==>ip_allocator
	INFO  ==>ip_atob, ip_address=192.168.1.7           //take network configuration
	INFO  ip_atob==>
	INFO  ==>ip_atob, ip_address=192.168.1.6
	INFO  ip_atob==>
	INFO  ==>ip_atob, ip_address=192.168.1.1
	INFO  ip_atob==>
	INFO  ==>ip_atob, ip_address=255.255.255.0
	INFO  ip_atob==>
	INFO  ==>ip_atob, ip_address=8.8.8.8
	INFO  ip_atob==>
	INFO  ==>ip_atob, ip_address=8.8.4.4
	INFO  ip_atob==>
	INFO  ip_allocator==>
	INFO  do_offer==>
	INFO  dispatch==>
	INFO  serialize==>, packet=552160                 //'encode' packet
	INFO  total 300 bytes written, ==>serialize
	INFO  ==>free_packet, packet=552160               //here packet is already sent and then is deleted from heap memory
	INFO  free_packet==>
	INFO  ==>free_packet, packet=551888
	INFO  free_packet==>
	INFO  handle_msg==>

## Authors

* **Marco Dalla Mutta** - [modm95](https://bitbucket.org/modm95/)

## License

This project is licensed under the MIT License and the use of cygwin1.dll is licensed under the LGPLv3 license - see the [LICENSE.md](LICENSE.md) file for details.

## Xtra

In Xtra folder you can find the RFC 2131 and 2132 text files, which are the base of DHCP and of this program, and you can also find the sources of cygwin1.dll.
